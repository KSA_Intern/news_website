"""news_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include, re_path
from . import views

app_name = "experiments"

urlpatterns = [
    re_path(
        route=r'^news/query/$',
        view=views.get_everything_by_query,
        name='get_everything_by_query',
    ),
    re_path(
        route=r'^news/feed/$',
        view=views.topic_feed,
        name='topic_feed', # HomePage
    ),
    re_path(
        route=r'^news/test/',
        view=views.test,
        name='test',
    ),
]