from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.conf import settings
from django.core.paginator import Paginator, Page, EmptyPage, PageNotAnInteger
from django.shortcuts import render

import time
import logging
import re
import os
import pandas as pd
from infinite_scroll_pagination import paginator
from infinite_scroll_pagination import serializers
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q, MultiSearch
from elasticsearch_dsl import connections
# from elasticsearch_dsl.connections import connections

from ..news.models import (Article, Topic, UserTopics, Related_Topic)
from ..news.utils.utils import (get_alternate_image_path, get_source)
from ..news.documents import ArticleDocument, es_search

# Load required CSV
abs_path = os.path.dirname(os.path.abspath(__file__))
blog_names = pd.read_csv(os.path.join(abs_path,"RSS Feeds - Blogs.csv"),encoding="UTF-8")["Name"].str.split().str.join(' ').str.lower().tolist()

# client = Elasticsearch()

# es_search = Search(using=client)

# # Create a connection to ElasticSearch
# # connections.configure(default=settings.ELASTICSEARCH_DSL['default'])
# connections.create_connection()


NEWS_HOME_USER_FOLLOWING_MESSAGE = "You are following '<<topic_name>>' but it looks like there are no news about the topic recently."

NEWS_HOME_USER_NOT_FOLLOWING_MESSAGE = "There are no news about '<<topic_name>>' now. Once you start following, you will be updated if any news comes."

NEWS_HOME_EMPTY_FEED_MESSAGE = "It looks like there are no news for the topics you are following. Stay tuned, you will be updated here if any news come. You may also add more topics in the meantime."

TOP_N_ARTICLES = 20

ARTICLES_PER_PAGE = 7

User = get_user_model()

logger = logging.getLogger(__name__)

# Create your views here.

def paginate(object_list, page_no, default_num_per_page = 20):
    paginator = Paginator(object_list, default_num_per_page)
    try:
        objects = paginator.page(page_no)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    
    return objects

def string_found(string1, string2):
        if re.search(r"\b" + re.escape(string1) + r"\b", string2):
            return True
        return False

@login_required
def topic_feed(request):
    if False: #not request.is_ajax() and request.method=="GET" and not 'page' in request.GET:
        return render(request,'news/news_home.html',context={"redirect_url":"/experiments/news/feed/"})
    else:
        start_time = time.time()
        if request.method == "POST":
            query = request.POST.get("query")
            logger.debug("Got the query - "+query)
            try:
                topic, if_created = Topic.objects.get_or_create(name=str(query).lower().strip())
                logger.info("New topic added to db as per user request - Topic "+topic.name)
            except Exception as identifier:
                logger.exception(identifier)
                topic = Topic.objects.get(name=str(query).lower().strip())
            
            user_topic = UserTopics(topic=topic, user=request.user)
            try:
                user_topic.save()
            except Exception as e:
                logger.exception(e)
            
            return redirect(reverse("news:get_everything_by_query")+"?query="+str(query).strip())
        
        if request.method == "GET":
            query = request.GET.get('query')
            query = str(query).lower().strip()
            page_no = request.GET.get('page', 1)
            try:
                value = None
                pk = None
                try:
                    # print("Read : ",request.GET.get('page', ''))
                    val_pk_dict = eval(request.GET.get('page', ''))
                    value = val_pk_dict['value'][0]
                    pk = val_pk_dict['pk']
                    
                except serializers.InvalidPage:
                    return Http404()
            except:
                pass
        else:
            page_no = 1

        # user_topics = UserTopics.objects.filter(user=request.user).select_related("topic")#.prefetch_related("topic__related_articles")
        # print(UserTopics.objects.filter(user=request.user).select_related("topic").prefetch_related("topic__related_articles").explain())
        # logger.info("These are the user topics")
        # logger.info(user_topics)
        all_topics = []
        user_topics = list(UserTopics.objects.filter(user=request.user).values_list('topic__name', flat=True))
        all_topics+=user_topics
        # Need to find a better way to sort articles from multiple topics
        # Using Category_Relation is a way
        all_articles = []
        article_topic_map = {}
        articles_info_map = {"articles":[]}
        related = Q()
        # ms = MultiSearch(index='articles')
        i = 0

        topic_subtopic_map = {}
        for user_topic in user_topics:
            i = i+1
            topic_subtopic_map[user_topic] = user_topic
            # new = user_topic.topic.related_articles.all()
            related_topic = Related_Topic.objects.filter(name=user_topic)
            if related_topic:
                related_topic = related_topic[0]
                rel_name = related_topic.name
                q = Q("match_phrase", title=rel_name)
                related_sub_topics = list(related_topic.topics.values_list('name', flat=True))
                # all_topics+=related_sub_topics
                for i_related_sub_topic in related_sub_topics:
                    q = q | Q("match_phrase", title=i_related_sub_topic)
                    all_topics.append(i_related_sub_topic)
                    topic_subtopic_map[i_related_sub_topic] = rel_name
            else:
                q = Q("match_phrase", title=user_topic)
            # new = ArticleDocument.search()[:2000].query(q).to_queryset()
            # new = user_topic.topic.related_articles.values_list("id",flat=True)[:20]
            # Get articles of related topic
            # try:
            #     related_topics = Related_Topic.objects.filter(name=str(user_topic.topic.name))#.prefetch_related("topics")
            #     # print(Article.objects.filter(id__in=Related_Topic.objects.filter(name=user_topic.topic.name).values_list("topics__related_articles",flat=True)))
            #     if related_topics:
            #         # logger.info("Related topics for query - "+str(query)+" is: ")
            #         # logger.info(related_topics.topics.all())
            #         # print(related_topics[0].topics.values_list("related_articles",flat=True))
            #         # new = new | Article.objects.filter(id__in=related_topics[0].topics.values_list("related_articles",flat=True))
            #         for related_topic in related_topics[0].topics.all():
            #             # new = new | related_topic.related_articles.all() 
            #             new = new | related_topic.related_articles.all()
            #             # new = new | related_topic.related_articles.values_list("id",flat=True)[:20]

            # except Exception as e:
            #     logger.error(e)
            #     pass
            if i ==1 :
                related = q
            else :
                related = related | q
            # if not related:
            #         logger.info("Topic {} has no articles".format(user_topic.topic.name))
            #         if settings.USE_GOOGLE_API:
            #             related = fetch_articles(str(user_topic.topic.name).lower().strip())

            # for article in new :
            #     # all_articles.append(article.id)
            #     if article.id in article_topic_map:
            #     # if article in article_topic_map:
            #         article_topic_map[article.id].append(user_topic.topic)
            #     else :
            #         article_topic_map[article.id] = [user_topic.topic] 
        
        # logger.info(f'Time till article_topic_map is made: {time.time() - start_time}')
        # required_articles = Article.objects.filter(id__in=all_articles)
        # print(f"Before distinct : {len(related)}")
        required_articles = ArticleDocument.search()[:i*1000].query(related).sort('-publishedAt')
        # required_articles = related.distinct().order_by('-publishedAt')
        # required_articles = Article.objects.filter(id__in=related).distinct()
        # print(f"After distinct : {len(required_articles)}")
        logger.info(f'Time till required_articles is made: {time.time() - start_time}')

        articles_info_list = paginate(required_articles, page_no, ARTICLES_PER_PAGE)
        # articles_info_list = paginate_infinite(required_articles, value, pk, ARTICLES_PER_PAGE)
        for article in articles_info_list:
            individual_article = dict()
            individual_article["article_obj"] = article
            # individual_article["related_topics"] = article.topic_set.all() & Topic.objects.filter(id__in=UserTopics.objects.filter(user=request.user).select_related("topic").values("topic__id"))
            # individual_article["related_topics"] = set(Article.objects.get(id=article.id).topic_set.all()).intersection(user_topics)
            individual_article["related_topics"] = [topic_subtopic_map[topic] for topic in all_topics if string_found(topic,article.title.lower())]
            # individual_article["related_topics"] =  article_topic_map[article.id]
            individual_article["alternate_img"] = get_alternate_image_path(str(article.url))
            individual_article["alternate_source"] = get_source(str(article.url))
            articles_info_map["articles"].append(individual_article)
        
        # logger.info(f'Time till for loop: {time.time() - start_time}')
        
        # articles_info_list = paginate(articles_info_map["articles"], page_no, ARTICLES_PER_PAGE)

        messages = []
        if not articles_info_map["articles"]:
            messages.append(NEWS_HOME_EMPTY_FEED_MESSAGE)
        
        # logger.info("These are the fetched articles to display")
        # logger.info(required_articles)

        # articles_and_topics = zip(required_articles, article_topics)
        # length = str(len(required_articles))
        # logger.info(f'Time till the end: {time.time() - start_time}')
        # logger.info("Number of articles : "+ length)

        # if request.is_ajax():
        #     html = render_to_string('news/news_home.html',{"articles_info_map":articles_info_map, "url": "topic_feed",
        #                                                 "articles_info_list":articles_info_list, "messages" : messages})
        #     return HttpResponse(html)

        
        return render(request, 'experiments/news_home.html',{"articles_info_map":articles_info_map, "url": "topic_feed",
                                                        "articles_info_list":articles_info_list, "messages" : messages})



@login_required
def get_everything_by_query(request):
    # [TASK] This function can be greatly improved. Topic model can be changed
    # to accomodate last queried time so that we can query the same query
    # from last time queried till now. For now, this is queried everytime
    # the user queries something
    start_time = time.time()
    query = ''
    if request.method == "GET":
        query = request.GET.get('query')
        query = str(query).lower().strip()
        page_no = request.GET.get('page', 1)
        try:
            value = None
            pk = None
            try:
                # print("Read : ",request.GET.get('page', ''))
                val_pk_dict = eval(request.GET.get('page', ''))
                value = val_pk_dict['value'][0]
                pk = val_pk_dict['pk']
                
            except serializers.InvalidPage:
                return Http404()
        except:
            pass

    # required_articles = []
    
    topic = Topic.objects.filter(name=str(query))
    usertopics = []
    required_articles = []
    # If the topic doesn't exist fetch newly
    if not topic:
        if False: # settings.USE_GOOGLE_API:
            required_articles = fetch_articles(query)
        else:
            required_articles = []
            topic_new, if_created = Topic.objects.get_or_create(name=str(query))
    
    if query: 
        topic_name = query
        # see if there is a blog with this name
        if topic_name in blog_names:
            required_articles = topic[0].related_articles.all()

        else:
            related_topic = Related_Topic.objects.filter(name=topic_name)

            if related_topic:
                related_topic = related_topic[0]
                rel_name = related_topic.name
                q = Q("match_phrase", title=rel_name)
                related_sub_topics = list(related_topic.topics.values_list('name', flat=True))
                # all_topics+=related_sub_topics
                for i_related_sub_topics in related_sub_topics:
                    q = q | Q("match_phrase", title=i_related_sub_topics)
            else:
                # q = Q("match", title=topic[0].name)
                words = topic_name.split()
                q = Q("match", title=words[0])
                for word in words[1:] :
                    q = q & Q("match", title=word)
            
            required_articles = ArticleDocument.search()[:2000].query(q).sort('-publishedAt')#.to_queryset().order_by("-publishedAt") # topic[0].related_articles.all()
            if required_articles.count()<1 and topic:
                required_articles = topic[0].related_articles.all()
            # required_topic = TopicDocument.search()[:1].query("match_phrase", name=topic[0].name).extra(size=100)
            # for req_topic in required_topic:
            #     required_articles = req_topic.related_articles
            #     print(len(required_articles))
            #     break
            # required_articles = required_topic[0].related_articles
            # required_articles.execute()
            # s = es_search.query("match", title=topic[0].name)
            # response = s.execute()
            # print(response)
            # for hit in required_articles:
            #     print(hit.publishedAt)
    
    # Get the topic which might be newly added
    topic, if_created = Topic.objects.get_or_create(name=str(query))

    # See if the user follows the topic. This part is actually not required
    # But it is being added assuming there might be some error where user is
    # following a topic even though it doesn't exist in database
    if topic:
        usertopics = UserTopics.objects.filter(user=request.user, topic=topic)

    # Get articles of related topic
    # try:
    #     related_topics = Related_Topic.objects.filter(name=str(query).lower()).prefetch_related("topics__related_articles")
    #     if related_topics:
    #         for related_topic in related_topics[0].topics.all():
    #             required_articles = required_articles | related_topic.related_articles.all()
            
    #         required_articles = required_articles.distinct()
    #         # print(len(required_articles))      

    # except Exception as e:
    #     logger.error(e)
    #     logger.info(query)
    
    # logger.info(f"Time till pagination starts: {time.time() - start_time}")
    articles_info_list = paginate(required_articles,page_no, ARTICLES_PER_PAGE)
    # articles_info_list = paginate_infinite(required_articles, value, pk, ARTICLES_PER_PAGE)
    # article_topics = [[topic]] * len(required_articles)
    # logger.info(f"Time till pagination: {time.time() - start_time}")
    articles_info_map = {"articles" : []}
    for article in articles_info_list:
        individual_article = dict()
        individual_article["article_obj"] = article
        individual_article["related_topics"] = [topic]
        individual_article["alternate_img"] = get_alternate_image_path(str(article.url))
        individual_article["alternate_source"] = get_source(str(article.url))
        articles_info_map["articles"].append(individual_article)
    # logger.info(f"Time till for loop: {time.time() - start_time}")

    messages = []

    if not articles_info_map["articles"]:
        if usertopics:
            message = NEWS_HOME_USER_FOLLOWING_MESSAGE.replace("<<topic_name>>",str(topic.name).capitalize())
            messages.append(message)
        
        else :
            message = NEWS_HOME_USER_NOT_FOLLOWING_MESSAGE.replace("<<topic_name>>",str(topic.name).capitalize())
            messages.append(message)

    
    # If the user already follows the topic, disables feature to show the topic and ask him to follow
    if usertopics:
        topic = []
    else :
        topic = [topic]
    
    # articles_zip = zip(required_articles, article_topics)
    # logger.info(f"The length: {len(required_articles)}")
    # print("*"*1000)
    # print()
    # logger.info(f"Time till the end: {time.time() - start_time}")
    # print()
    return render(request, 'experiments/news_home.html',{"articles_info_list":articles_info_list, "topics" : topic,
                                                    'messages' : messages, "url":"get_everything_by_query",#'next_page': serializers.to_page_key(**articles_info_list.next_page()),
                                                    "searched_query":query, "articles_info_map":articles_info_map})


def test(request):
    logger.info("*"*200)
    wanted = ["Test URL"]

    wanted = blog_names
    

    return render(request, 'news/test.html',{"skipped" : wanted})