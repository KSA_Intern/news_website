from celery import task
from celery.task.schedules import crontab
from celery.decorators import periodic_task, task
from django.conf import settings
from django.http import  HttpResponseNotFound
from datetime import timedelta
import time

from .models import Topic, Source, TimeLogger
from .utils.utils import add_articles_to_database, add_rss_articles_to_db, add_rss_articles_by_query
from .utils.task import (TaskImplementor, CompanyAdditionTask, CompanySectorAdditionTask, DownloadGoogleSheetTask,
                         RelatedTopicMergeTask, SectorRSSAdditionTask, ArticleCompanyLinkingTask, BlogTopicsAdditionTask)
from .utils.pipelines import BlogUpdatePipeline

from newsapi import NewsApiClient
import logging

logger = logging.getLogger(__name__)

newsapi = NewsApiClient(api_key=settings.GOOGLE_NEWS_API_KEY)

company_addition_task = CompanyAdditionTask()

def fetch_news_for_all_topics():
    '''
        This fetches fresh news from google news API
        for all the topics that are in the database.
    '''
    topics = Topic.objects.all()

    for topic in topics:
        if "headline" in str(topic.name).lower() :
            continue
        
        logger.info("Fetching articles for "+topic.name)
        all_articles = newsapi.get_everything(q=str(topic.name).lower().strip(),
                                        language='en', page_size=21)
        if all_articles['status'].lower() != "ok" :
            continue

        add_articles_to_database(all_articles['articles'], query=str(topic.name).lower().strip())


# def update_topics_periodically():
#     logger.info("Starting periodic fetching of topics")
#     if settings.USE_GOOGLE_API:
#         fetch_news_for_all_topics()
#     else:
#         pass


def get_all_sources():
    sources = newsapi.get_sources()
    # skipped = []

    if sources['status'].lower() != "ok" :
        # [TASK] Need an error page
        return HttpResponseNotFound('<h4>'+ str(sources['message']) +'</h4>')

    for source in sources['sources']:
        source_obj = Source()
        for key, value in source.items():
            if key == 'id':
                setattr(source_obj,'api_id',value)
            else :
                setattr(source_obj,key,value)
        
        try:
            source_obj.save()
        except Exception as e:
            logger.exception(e)

@periodic_task(run_every=crontab(hour="13", minute="0", day_of_week="*"))
def fetch_sources_periodically():
    if settings.USE_GOOGLE_API:
        get_all_sources()
    else :
        pass


@periodic_task(run_every=timedelta(minutes=60), soft_time_limit=3550)
def update_rss_topics_periodically():
    logger.info("Starting periodic fetching")
    start_time = time.time()

    # start = time.time()
    # implementor = TaskImplementor()
    # implementor.register(CompanySectorAdditionTask())
    # implementor.implement()
    # try:
    #     timelog5, if_created = TimeLogger.objects.get_or_create(name="CompanySectorAdditionTask-finish time")
    #     if not timelog5.time_lapsed or timelog5.time_lapsed < time.time() - start:
    #         timelog5.time_lapsed = time.time() - start
    #     timelog5.save()
    # except Exception as e:
    #     logger.error(e)

    start = time.time()
    implementor = TaskImplementor()
    implementor.register(DownloadGoogleSheetTask())
    implementor.implement()
    try:
        timelog1, if_created = TimeLogger.objects.get_or_create(name="DownloadGoogleSheetTask-finish time")
        if not timelog1.time_lapsed or timelog1.time_lapsed < time.time() - start:
            timelog1.time_lapsed = time.time() - start
        timelog1.save()
    except Exception as e:
        logger.error(e)

    # start = time.time()
    # implementor = TaskImplementor()
    # implementor.register(CompanySectorAdditionTask())
    # implementor.implement()
    # try:
    #     timelog5, if_created = TimeLogger.objects.get_or_create(name="CompanySectorAdditionTask-finish time")
    #     if not timelog5.time_lapsed or timelog5.time_lapsed < time.time() - start:
    #         timelog5.time_lapsed = time.time() - start
    #     timelog5.save()
    # except Exception as e:
    #     logger.error(e)

    implementor = TaskImplementor()
    implementor.register(SectorRSSAdditionTask())
    start = time.time()
    implementor.implement()
    try:
        timelog, if_created = TimeLogger.objects.get_or_create(name="SectorRSSAdditionTask-finish time")
        if not timelog.time_lapsed or timelog.time_lapsed < time.time() - start:
            timelog.time_lapsed = time.time() - start
        timelog.save()
    except Exception as e:
        logger.error(e)
    
    start = time.time()
    implementor = TaskImplementor()
    implementor.register(RelatedTopicMergeTask())
    implementor.implement()
    try:
        timelog3, if_created = TimeLogger.objects.get_or_create(name="RelatedTopicMergeTask-finish time")
        if not timelog3.time_lapsed or timelog3.time_lapsed < time.time() - start:
            timelog3.time_lapsed = time.time() - start
        timelog3.save()
    except Exception as e:
        logger.error(e)

    start = time.time()
    implementor = TaskImplementor()
    implementor.register(company_addition_task)
    implementor.implement()
    try:
        timelog4, if_created = TimeLogger.objects.get_or_create(name="CompanyAdditionTask-finish time")
        if not timelog4.time_lapsed or timelog4.time_lapsed < time.time() - start:
            timelog4.time_lapsed = time.time() - start
        timelog4.save()
    except Exception as e:
        logger.error(e)

    # logger.info("*"*300)
    # logger.info("\n"*5)
    logger.info(f"Scheduling completed.\nTotal time taken for scheduling is {time.time()-start_time}s.")
    # logger.info("\n"*5)
    # logger.info("*"*300)

@periodic_task(run_every=timedelta(minutes=111), soft_time_limit=7200)
def fetch_company_related_news():
    start = time.time()
    add_rss_articles_by_query()
    try:
        timelog2, if_created = TimeLogger.objects.get_or_create(name="add_rss_articles_by_query-finish time")
        if not timelog2.time_lapsed or timelog2.time_lapsed < time.time() - start:
            timelog2.time_lapsed = time.time() - start
        timelog2.save()
    except Exception as e:
        logger.error(e)

    start = time.time()
    blog_pipeline = BlogUpdatePipeline()
    blog_pipeline.run()
    try:
        timelog2, if_created = TimeLogger.objects.get_or_create(name="BlogUpdatePipeline-finish time")
        if not timelog2.time_lapsed or timelog2.time_lapsed < time.time() - start:
            timelog2.time_lapsed = time.time() - start
        timelog2.save()
    except Exception as e:
        logger.error(e)


@periodic_task(run_every=timedelta(minutes=120), soft_time_limit=7200)
def run_once_only_tasks():
    '''
        I always wanted a few tasks to run once from the point they were put to
        production but didn't know how. So now I am using this technique where
        I store the information about if they have run ever and check it periodically.
        If they have never been run, I run these tasks.

        These are very while setting up anything newly
    '''
    start = time.time()
    try:
        has_run_bool, if_run = TimeLogger.objects.get_or_create(name="BlogTopicsAdditionTask-if run")
        # logger.info("*"*1000)
        logger.info(has_run_bool.time_lapsed)
        logger.info(if_run)
        if if_run or not has_run_bool.time_lapsed:
            # logger.info("*"*1000)
            has_run_bool.time_lapsed = 1
            has_run_bool.save()
            start = time.time()
            implementor = TaskImplementor()
            implementor.register(BlogTopicsAdditionTask())
            implementor.implement()
            try:
                timelog2, if_created = TimeLogger.objects.get_or_create(name="BlogTopicsAdditionTask-finish time")
                if not timelog2.time_lapsed or timelog2.time_lapsed < time.time() - start:
                    timelog2.time_lapsed = time.time() - start
                timelog2.save()
            except Exception as e:
                logger.error(e)
    except Exception as e:
        logger.error(e)

    # start = time.time()
    # try:
    #     has_run_bool, if_run = TimeLogger.objects.get_or_create(name="RevertBackTheBlunder-if run")
    #     # logger.info("*"*1000)
    #     logger.info(has_run_bool.time_lapsed)
    #     logger.info(if_run)
    #     if if_run or not has_run_bool.time_lapsed:
    #         # logger.info("*"*1000)
    #         has_run_bool.time_lapsed = 1
    #         has_run_bool.save()
    #         start = time.time()
    #         implementor = TaskImplementor()
    #         implementor.register(RevertBackTheBlunder())
    #         implementor.implement()
    #         try:
    #             timelog2, if_created = TimeLogger.objects.get_or_create(name="RevertBackTheBlunder-finish time")
    #             if not timelog2.time_lapsed or timelog2.time_lapsed < time.time() - start:
    #                 timelog2.time_lapsed = time.time() - start
    #             timelog2.save()
    #         except Exception as e:
    #             logger.error(e)
    # except Exception as e:
    #     logger.error(e)

@task(name="add_sector_company_info", soft_time_limit=3600)
def add_sector_company_info():
    # implementor = TaskImplementor()
    # implementor.register(CompanyRSSAdditionTask())
    # implementor.implement()
    pass
