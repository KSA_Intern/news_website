from django import template

register = template.Library()

@register.filter(name='format_my_date_string')
def format_my_date_string(value):
    value = str(value)
    return value[8:10]+"-"+value[5:7]+"-"+value[0:4]#+" "+value[11:16]


@register.filter(name='replace_space_with_plus')
def replace_space_with_plus(value):
    value = str(value)
    retval = value.replace(" ","%2B")
    return retval