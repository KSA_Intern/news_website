import spacy
import logging
logger = logging.getLogger(__name__)

try:
    nlp = spacy.load("en_core_web_sm")

except Exception as e:
    os.system("python -m spacy download en_core_web_sm")
    nlp = spacy.load("en_core_web_sm")

# https://spacy.io/api/annotation#named-entities
ACCEPTED_LABELS_AS_TOPICS = ['PERSON', 'NORP', 'FAC', 'ORG', 'GPE', 'LOC', 'PRODUCT', 'EVENT','WORK_OF_ART', 'LAW',
                                'PER', 'MISC']

ACCEPTED_POS_AS_TOPICS = ['PROPN']

class SentenceProcessor(object):
    """
    docstring
    """
    def __init__(self):
        pass

    def filter_named_entities(self, sentence):
        '''
            Filters named entities from a sentence and returns
            tokens of words that are in accepted set
        '''

        doc = nlp(sentence)

        filtered_sentence = []
        
        for ent in doc.ents:
            if ent.label_ in ACCEPTED_LABELS_AS_TOPICS:
                filtered_sentence.append(ent.text)
        
        for token in doc:
            if token.pos_ in ACCEPTED_POS_AS_TOPICS and not token.text in filtered_sentence:
                filtered_sentence.append(token.text)
        
        return filtered_sentence