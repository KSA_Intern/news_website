from elasticsearch_dsl.connections import connections
from django_elasticsearch_dsl import Document, Index, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from .models import Article, Topic

client = Elasticsearch()

es_search = Search(using=client)

# Create a connection to ElasticSearch
connections.create_connection()

@registry.register_document
class ArticleDocument(Document):

    publishedAt = fields.KeywordField()
    id = fields.KeywordField()
    source = fields.ObjectField(properties={
        'id' : fields.KeywordField(),
        'name': fields.TextField(),
        'country': fields.TextField(),
    })
    class Index:
        # Name of the Elasticsearch index
        name = 'articles'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Article # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'title',
            'description',
            # 'publishedAt',
            'url',
            'urlToImage',
            'tags'
        ]


# @registry.register_document
# class TopicDocument(Document):

#     id = fields.KeywordField()
#     related_articles = fields.NestedField(properties={
#         'title': fields.TextField(),
#         'url': fields.TextField(),
#         'urlToImage': fields.TextField(),
#         'publishedAt': fields.KeywordField(),
#         'id': fields.KeywordField(),
#     })
#     class Index:
#         # Name of the Elasticsearch index
#         name = 'topics'
#         # See Elasticsearch Indices API reference for available settings
#         settings = {'number_of_shards': 1,
#                     'number_of_replicas': 0}

#     class Django:
#         model = Topic # The model associated with this Document

#         # The fields of the model you want to be indexed in Elasticsearch
#         fields = [
#             'name'
#         ]

#         related_models = [Article]