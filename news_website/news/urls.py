"""news_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include, re_path
from . import views

app_name = "news"

urlpatterns = [
    re_path(
        route=r'^$',
        view=views.topic_feed,
        name='home',
    ),
    path(
        'follow/<topicname>/',
        view=views.get_topic_by_query,
        name='get_topic_by_query',
    ),
    re_path(
        route=r'^analytics/admin/$',
        view=views.analytics_page,
        name='analytics_page',
    ),
    # re_path(
    #     route=r'^news/$',
    #     view=views.topic_feed,
    #     name='search_page',
    # ),
    re_path(
        route=r'^news/sources/$',
        view=views.get_all_sources,
        name='get_all_sources',
    ),
    re_path(
        route=r'^news/discovery/$',
        view=views.discovery_page,
        name='discovery_page',
    ),

    re_path(
        route=r'^news/discovery/follow/$',
        view=views.follow_topic,
        name='follow_topic',
    ),
    re_path(
        route=r'^news/discovery/(?P<item>[\w-]+)/$',
        view=views.discover_each_item,
        name='discover_each_item',
    ),

    re_path(
        route=r'^news/top/$',
        view=views.TopHeadlinesListView.as_view(),
        name='top_headlines',
    ),
    re_path(
        route=r'^news/query/$',
        view=views.get_everything_by_query,
        name='get_everything_by_query',
    ),
    re_path(
        route=r'^news/add/$',
        view=views.landing_page,
        name='landing_page',
    ),
    re_path(
        route=r'^news/feed/$',
        view=views.topic_feed,
        name='topic_feed', # HomePage
    ),
    re_path(
        route=r'^news/unfollow/$',
        view=views.unfollow_topic,
        name='unfollow_topic',
    ),
    re_path(
        route=r'^concalls/$',
        view=views.concalls_view,
        name='concalls_view',
    ),
    re_path(
        route=r'^trending/$',
        view=views.trending_view,
        name='trending_view',
    ),
    re_path(
        route=r'^news/autocomplete/$',
        view=views.autocomplete_home_page_text,
        name='autocomplete_home_page_text',
    ),
    # re_path(
    #     route=r'^query/(?P<query>\w+)/$',
    #     view=views.get_everything_by_query,
    #     name='get_everything_by_query',
    # ),
    re_path(
        route=r'^news/test/',
        view=views.test,
        name='test',
    ),
    
    ###############
    # Blogs

    re_path(
        route=r'^blogs/news/page/$',
        view=views.blogs_news_page,
        name='blogs_news_page',
    ),

    re_path(
        route=r'^blogs/news/page/test',
        view=views.blogs_test,
        name='blogs_test',
    ),
    
    re_path(
        route=r'^api/blogs/topics/follow',
        view=views.blogs_follow_topic,
        name='blogs_follow_topic',
    ),

    re_path(
        route=r'^api/blogs/topics/unfollow',
        view=views.blogs_unfollow_topic,
        name='blogs_unfollow_topic',
    ),

    re_path(
        route=r'^api/blogs/topics/autocomplete',
        view=views.blogs_autocomplete_topic,
        name='blogs_autocomplete_topic',
    ),

    re_path(
        route=r'^api/blogs/topics/getAllFollowing/',
        view=views.blogs_get_all_user_following_topics,
        name='blogs_get_all_user_following_topics',
    ),
    re_path(
        route=r'^api/blogs/news/feed/',
        view=views.blogs_fetch_news_articles,
        name='blogs_fetch_news_articles',
    ),
]