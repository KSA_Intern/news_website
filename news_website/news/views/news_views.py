import enum
from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.apps import AppConfig
from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.template.loader import render_to_string
from django.conf import settings
from django.views.generic import ListView, TemplateView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count, F

from ..models import (Source, Article, Category_Relation, Topic, UserTopics, DefaultTopic, Related_Topic,
                         Topic_Cluster, Company, TimeLogger)
from ..utils.utils import (add_articles_to_database, get_rss_news, add_rss_articles_to_db, month_to_num,
                    get_alternate_image_path, get_source, found_in_allowed_urls)
from ..tasks import add_sector_company_info
from ...news.documents import ArticleDocument

from infinite_scroll_pagination import paginator
from infinite_scroll_pagination import serializers
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q, MultiSearch
from elasticsearch_dsl import connections
from newsapi import NewsApiClient
from datetime import datetime, date
import pandas as pd
import time
import re
import logging
import operator
import json
import os

NEWS_HOME_USER_FOLLOWING_MESSAGE = "You are following '<<topic_name>>' but it looks like there are no news about the topic recently."

NEWS_HOME_USER_NOT_FOLLOWING_MESSAGE = "There are no news about '<<topic_name>>' now. Once you start following, you will be updated if any news comes."

NEWS_HOME_EMPTY_FEED_MESSAGE = "It looks like there are no news for the topics you are following. Stay tuned, you will be updated here if any news come. You may also add more topics in the meantime."

TOP_N_ARTICLES = 20

ARTICLES_PER_PAGE = 7

User = get_user_model()

abs_path = os.path.dirname(os.path.abspath(__file__))
blog_names = pd.read_csv(os.path.join(abs_path,"../RSS Feeds - Blogs.csv"),encoding="UTF-8")["Name"].str.split().str.join(' ').str.lower().tolist()
discover_companies = pd.read_csv(os.path.join(abs_path,"../Discovery Page - Companies.csv"),encoding="UTF-8")["Items"].str.split().str.join(' ').str.lower().tolist()
discover_sectors = pd.read_csv(os.path.join(abs_path,"../Discovery Page - Sectors.csv"),encoding="UTF-8")["Items"].str.split().str.join(' ').str.lower().tolist()
discover_topics = pd.read_csv(os.path.join(abs_path,"../Discovery Page - Topics.csv"),encoding="UTF-8")["Items"].str.split().str.join(' ').str.lower().tolist()
# concalls = pd.read_csv(os.path.join(abs_path,"Concalls Today.csv"),encoding="UTF-8")
# [TASK] For all the queries need to implement loop to query 
# Now only 20 per page is being shown
# [TASK] Need to remove skipped variable
# [TASK] There needs to be more processing on the query that
#        people search for. For example, now 'The Microsoft',
#        'Microsoft' are different topic searches. Such 
#        Such cases needs to be handled

logger = logging.getLogger(__name__)

# Utils


def fetch_articles(query):
    '''
        This fetches articles from Google API
    '''

    logger.info("Fetching using Google API")
    newsapi = NewsApiClient(api_key=settings.GOOGLE_NEWS_API_KEY)
    try:
        all_articles = newsapi.get_everything(q=str(query),
                                        language='en', page_size=21)
    except Exception as e:
        logger.error(e)
        return []
        # return HttpResponseNotFound('<h4>'+ 'Sorry for the inconvenience. We will work on the problem soon.' +'</h4>')

    if all_articles['status'].lower() != "ok" :
        # [TASK] Need an error page
        return []
        return HttpResponseNotFound('<h4>'+ str(all_articles['message']) +'</h4>')

    logger.info("Adding articles to database now. Starting...")
    add_articles_to_database(all_articles['articles'], query=str(query).lower().strip())
    logger.info("Successfully added articles to database")
    topic = Topic.objects.filter(name=str(query).lower().strip())

    if topic:
        required_articles = topic[0].related_articles.all()
    
    else :
        required_articles = []
        
    logger.debug("The articles are : {}".format(required_articles))

    return required_articles



# Create your views here.

@login_required
def landing_page(request):
    if request.method == "POST":
        query = request.POST.get("topic")
        logger.debug("Entered landing page")
        try:
            topic, if_created = Topic.objects.get_or_create(name=str(query).lower().strip())
            logger.info("New topic added to db as per user request - Topic "+topic.name)
        except Exception as identifier:
            logger.exception(identifier)
            topic = Topic.objects.get(name=str(query).lower().strip())
        
        user_topic, if_created = UserTopics.objects.get_or_create(topic=topic, user=request.user)
        try:
            user_topic.save()
        except Exception as e:
            logger.exception(e)
        

    user_topics = UserTopics.objects.filter(user=request.user).order_by('-topic__name')
    default_t = DefaultTopic.objects.all()

    default_topics = []
    for default_topic in default_t :
        if_found = False
        for user_topic in user_topics:
            if (str(user_topic.topic.name).lower() == str(default_topic.topic.name).lower()):
                if_found = True
                break
        if not if_found :
            default_topics.append(default_topic)
        else :
            pass

    if not user_topics:
        logger.debug("User {} doesn't have any topics following".format(request.user.email))
    
    context = {"user_topics":user_topics, "default_topics" : default_topics}

    return render(request, 'news/landing_page.html',context)

@login_required
def unfollow_topic(request):
    ''' Unfollow a topic '''

    if request.method == "POST":
        topic_query = request.POST.get("topic")
        topic = Topic.objects.get(name=str(topic_query).lower().strip())
        user_topic = get_object_or_404(UserTopics, user=request.user, topic=topic)
        user_topic.delete()

    return redirect("news:landing_page")       


def paginate(object_list, page_no, default_num_per_page = 20):
    paginator = Paginator(object_list, default_num_per_page)
    try:
        objects = paginator.page(page_no)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    
    return objects

def paginate_infinite(objects_query, value, pk, ARTICLES_PER_PAGE):
    try:
        page = paginator.paginate(
            query_set=objects_query,
            value=value,
            pk=pk,
            lookup_field='-publishedAt',
            per_page=ARTICLES_PER_PAGE,
            move_to=paginator.NEXT_PAGE)
    except paginator.EmptyPage:
        data = {'error': "this page is empty"}
    
    return page
def string_found(string1, string2):
        if re.search(r"\b" + re.escape(string1) + r"\b", string2):
            return True
        return False


# TODO The number of articles fetched now is basically number_of_topics_followed*1000. After that 
#      the articles end. Need to change this to dynamically fetch the next set as and when it's over
@login_required
def topic_feed(request):
    if not request.is_ajax() and request.method=="GET" and not 'page' in request.GET:
        return render(request,'news/news_home.html',context={"redirect_url":"/news/feed/"})
    else:
        start_time = time.time()
        if request.method == "POST":
            query = request.POST.get("query")
            logger.debug("Got the query - "+query)
            try:
                topic, if_created = Topic.objects.get_or_create(name=str(query).lower().strip())
                logger.info("New topic added to db as per user request - Topic "+topic.name)
            except Exception as identifier:
                logger.exception(identifier)
                topic = Topic.objects.get(name=str(query).lower().strip())
            
            user_topic = UserTopics(topic=topic, user=request.user)
            try:
                user_topic.save()
            except Exception as e:
                logger.exception(e)
            
            return redirect(reverse("news:get_everything_by_query")+"?query="+str(query).strip())
        
        if request.method == "GET":
            query = request.GET.get('query')
            query = str(query).lower().strip()
            page_no = request.GET.get('page', 1)
            try:
                value = None
                pk = None
                try:
                    # print("Read : ",request.GET.get('page', ''))
                    val_pk_dict = eval(request.GET.get('page', ''))
                    value = val_pk_dict['value'][0]
                    pk = val_pk_dict['pk']
                    
                except serializers.InvalidPage:
                    return Http404()
            except:
                pass
        else:
            page_no = 1

        # user_topics = UserTopics.objects.filter(user=request.user).select_related("topic")#.prefetch_related("topic__related_articles")
        # print(UserTopics.objects.filter(user=request.user).select_related("topic").prefetch_related("topic__related_articles").explain())
        # logger.info("These are the user topics")
        # logger.info(user_topics)
        # user_topics_list = UserTopics.objects.filter(user=request.user).values_list('topic__name', flat=True)
        all_topics = []
        user_topics = list(UserTopics.objects.filter(user=request.user).values_list('topic__name', flat=True))
        all_topics+=user_topics
        # Need to find a better way to sort articles from multiple topics
        # Using Category_Relation is a way
        all_articles = []
        article_topic_map = {}
        articles_info_map = {"articles":[]}
        related = Q()
        # ms = MultiSearch(index='articles')
        i = 0
        
        topic_subtopic_map = {}
        for user_topic in user_topics:
            i = i+1
            topic_subtopic_map[user_topic] = user_topic
            # new = user_topic.topic.related_articles.all()
            related_topic = Related_Topic.objects.filter(name=user_topic)
            if related_topic:
                related_topic = related_topic[0]
                rel_name = related_topic.name
                q = Q("match_phrase", title=rel_name)
                related_sub_topics = list(related_topic.topics.values_list('name', flat=True))
                # all_topics+=related_sub_topics
                for i_related_sub_topic in related_sub_topics:
                    q = q | Q("match_phrase", title=i_related_sub_topic)
                    all_topics.append(i_related_sub_topic)
                    topic_subtopic_map[i_related_sub_topic] = rel_name
            else:
                q = Q("match_phrase", title=user_topic)
            # new = user_topic.topic.related_articles.values_list("id",flat=True)[:20]
            # Get articles of related topic
            # try:
            #     related_topics = Related_Topic.objects.filter(name=str(user_topic.topic.name))#.prefetch_related("topics")
            #     # print(Article.objects.filter(id__in=Related_Topic.objects.filter(name=user_topic.topic.name).values_list("topics__related_articles",flat=True)))
            #     if related_topics:
            #         # logger.info("Related topics for query - "+str(query)+" is: ")
            #         # logger.info(related_topics.topics.all())
            #         # print(related_topics[0].topics.values_list("related_articles",flat=True))
            #         # new = new | Article.objects.filter(id__in=related_topics[0].topics.values_list("related_articles",flat=True))
            #         for related_topic in related_topics[0].topics.all():
            #             # new = new | related_topic.related_articles.all() 
            #             new = new | related_topic.related_articles.all()
            #             # new = new | related_topic.related_articles.values_list("id",flat=True)[:20]

            # except Exception as e:
            #     logger.error(e)
            #     pass
            if i == 1 :
                related = q
            else :
                related = related | q
            # if not related:
            #         logger.info("Topic {} has no articles".format(user_topic.topic.name))
            #         if settings.USE_GOOGLE_API:
            #             related = fetch_articles(str(user_topic.topic.name).lower().strip())

            # for article in new :
            #     # all_articles.append(article.id)
            #     if article.id in article_topic_map:
            #     # if article in article_topic_map:
            #         article_topic_map[article.id].append(user_topic.topic)
            #     else :
            #         article_topic_map[article.id] = [user_topic.topic] 
        
        # logger.info(f'Time till article_topic_map is made: {time.time() - start_time}')
        # required_articles = Article.objects.filter(id__in=all_articles)
        # print(f"Before distinct : {len(related)}")
        required_articles = ArticleDocument.search()[:i*1000].query(related).sort('-publishedAt')
        # required_articles = Article.objects.filter(id__in=related).distinct()
        # print(f"After distinct : {len(required_articles)}")
        # logger.info(f'Time till required_articles is made: {time.time() - start_time}')

        articles_info_list = paginate(required_articles, page_no, ARTICLES_PER_PAGE)
        # articles_info_list = paginate_infinite(required_articles, value, pk, ARTICLES_PER_PAGE)
        for article in articles_info_list:
            individual_article = dict()
            individual_article["article_obj"] = article
            # individual_article["related_topics"] = article.topic_set.all() & Topic.objects.filter(id__in=UserTopics.objects.filter(user=request.user).select_related("topic").values("topic__id"))
            # individual_article["related_topics"] = set(Article.objects.get(id=article.id).topic_set.all()).intersection(user_topics)
            # individual_article["related_topics"] = article_topic_map[article.id]
            # individual_article["related_topics"] = set(article.topic_set.values_list("name",flat=True)).intersection(user_topics_list)
            individual_article["related_topics"] = list(set([topic_subtopic_map[topic] for topic in all_topics if (string_found(topic,article.title.lower()))]))
            # print(individual_article["related_topics"])
            individual_article["alternate_img"] = get_alternate_image_path(str(article.url))
            individual_article["alternate_source"] = get_source(str(article.url))
            articles_info_map["articles"].append(individual_article)
        
        # logger.info(f'Time till for loop: {time.time() - start_time}')
        
        # articles_info_list = paginate(articles_info_map["articles"], page_no, ARTICLES_PER_PAGE)

        messages = []
        if not articles_info_map["articles"]:
            messages.append(NEWS_HOME_EMPTY_FEED_MESSAGE)

        
        # trending_topics = pd.read_csv(os.path.join(abs_path,"utils/Trending Topics.csv"),encoding="UTF-8")
        # disp_name = trending_topics['Display name'].values
        # image_url = trending_topics['Image URL'].values
        # search_term = trending_topics['Search Term'].values
        trending_topics_list = []
        # for i, name in enumerate(disp_name):
        #     temp_dict = {}
        #     temp_dict["disp_name"] = name
        #     temp_dict["image_url"] = image_url[i]
        #     temp_dict["search_term"] = search_term[i]
        #     trending_topics_list.append(temp_dict)
        
        # logger.info("These are the fetched articles to display")
        # logger.info(required_articles)

        # articles_and_topics = zip(required_articles, article_topics)
        # length = str(len(required_articles))
        # logger.info(f'Time till the end: {time.time() - start_time}')
        # logger.info("Number of articles : "+ length)

        # if request.is_ajax():
        #     html = render_to_string('news/news_home.html',{"articles_info_map":articles_info_map, "url": "topic_feed",
        #                                                 "articles_info_list":articles_info_list, "messages" : messages})
        #     return HttpResponse(html)

        
        return render(request, 'news/news_home.html',{"articles_info_map":articles_info_map, "url": "topic_feed",
                                                        "articles_info_list":articles_info_list, "messages" : messages,
                                                        'trending_topics_list':trending_topics_list})


@login_required
def get_everything_by_query(request):
    # [TASK] This function can be greatly improved. Topic model can be changed
    # to accomodate last queried time so that we can query the same query
    # from last time queried till now. For now, this is queried everytime
    # the user queries something
    start_time = time.time()
    query = ''
    if request.method == "GET":
        query = request.GET.get('query')
        query = str(query).lower().strip()
        page_no = request.GET.get('page', 1)
        try:
            value = None
            pk = None
            try:
                # print("Read : ",request.GET.get('page', ''))
                val_pk_dict = eval(request.GET.get('page', ''))
                value = val_pk_dict['value'][0]
                pk = val_pk_dict['pk']
                
            except serializers.InvalidPage:
                return Http404()
        except:
            pass

    # required_articles = []
    topic = Topic.objects.filter(name=str(query))
    usertopics = []
    required_articles = []
    # If the topic doesn't exist fetch newly
    if not topic:
        if settings.USE_GOOGLE_API:
            required_articles = fetch_articles(query)
        else:
            required_articles = []
            topic, if_created = Topic.objects.get_or_create(name=str(query))
    
    topic = Topic.objects.filter(name=str(query))
    # else: # If the topic exists fetch articles and check if user follows the topic
    #     required_articles = topic[0].related_articles.all()
    #     #If the topic doesn't exist in the database, then user can't follow it
    #     usertopics = UserTopics.objects.filter(user=request.user, topic=topic[0])


    if query:
        topic_name = query
        # see if there is a blog with this name
        if topic_name in blog_names:
            required_articles = topic[0].related_articles.all()

        else:
            related_topic = Related_Topic.objects.filter(name=topic_name)

            if related_topic:
                related_topic = related_topic[0]
                rel_name = related_topic.name
                q = Q("match_phrase", title=rel_name)
                related_sub_topics = list(related_topic.topics.values_list('name', flat=True))
                # all_topics+=related_sub_topics
                for i_related_sub_topics in related_sub_topics:
                    q = q | Q("match_phrase", title=i_related_sub_topics)
            else:
                # q = Q("match", title=topic[0].name)
                words = topic_name.split()
                q = Q("match", title=words[0])
                for word in words[1:] :
                    q = q & Q("match", title=word)
            
            required_articles = ArticleDocument.search()[:2000].query(q).sort('-publishedAt')#.to_queryset().order_by("-publishedAt") # topic[0].related_articles.all()
            if required_articles.count()<1 and topic:
                required_articles = topic[0].related_articles.all()
            # required_topic = TopicDocument.search()[:1].query("match_phrase", name=topic[0].name).extra(size=100)
            # for req_topic in required_topic:
            #     required_articles = req_topic.related_articles
            #     print(len(required_articles))
            #     break
            # required_articles = required_topic[0].related_articles
            # required_articles.execute()
            # s = es_search.query("match", title=topic[0].name)
            # response = s.execute()
            # print(response)
            # for hit in required_articles:
            #     print(hit.publishedAt)

    # Get the topic which might be newly added
    topic, if_created = Topic.objects.get_or_create(name=str(query))

    # See if the user follows the topic. This part is actually not required
    # But it is being added assuming there might be some error where user is
    # following a topic even though it doesn't exist in database
    if topic:
        usertopics = UserTopics.objects.filter(user=request.user, topic=topic)

    # Get articles of related topic
    # try:
    #     related_topics = Related_Topic.objects.filter(name=str(query).lower()).prefetch_related("topics__related_articles")
    #     if related_topics:
    #         for related_topic in related_topics[0].topics.all():
    #             required_articles = required_articles | related_topic.related_articles.all()
            
    #         required_articles = required_articles.distinct()
    #         # print(len(required_articles))      

    # except Exception as e:
    #     logger.error(e)
    #     logger.info(query)
    
    # logger.info(f"Time till pagination starts: {time.time() - start_time}")

    articles_info_list = paginate(required_articles,page_no, ARTICLES_PER_PAGE)
    # articles_info_list = paginate_infinite(required_articles, value, pk, ARTICLES_PER_PAGE)
    # article_topics = [[topic]] * len(required_articles)
    # logger.info(f"Time till pagination: {time.time() - start_time}")
    articles_info_map = {"articles" : []}
    for article in articles_info_list:
        individual_article = dict()
        individual_article["article_obj"] = article
        individual_article["related_topics"] = [topic]
        individual_article["alternate_img"] = get_alternate_image_path(str(article.url))
        individual_article["alternate_source"] = get_source(str(article.url))
        articles_info_map["articles"].append(individual_article)
    # logger.info(f"Time till for loop: {time.time() - start_time}")

    messages = []

    if not articles_info_map["articles"]:
        if usertopics:
            message = NEWS_HOME_USER_FOLLOWING_MESSAGE.replace("<<topic_name>>",str(topic.name).capitalize())
            messages.append(message)
        
        else :
            message = NEWS_HOME_USER_NOT_FOLLOWING_MESSAGE.replace("<<topic_name>>",str(topic.name).capitalize())
            messages.append(message)

    
    # If the user already follows the topic, disables feature to show the topic and ask him to follow
    if usertopics:
        topic = []
    else :
        topic = [topic]
    
    # articles_zip = zip(required_articles, article_topics)
    # logger.info(f"The length: {len(required_articles)}")
    # print("*"*1000)
    # print()
    # logger.info(f"Time till the end: {time.time() - start_time}")
    # print()
    return render(request, 'news/news_home.html',{"articles_info_list":articles_info_list, "topics" : topic,
                                                    'messages' : messages, "url":"get_everything_by_query",#'next_page': serializers.to_page_key(**articles_info_list.next_page()),
                                                    "searched_query":query, "articles_info_map":articles_info_map})

def get_topic_by_query(request, topicname):
    # if not request.is_ajax() and request.method=="GET" and not 'page' in request.GET:
    if False: # Removing loading page and directly provide the page
        return render(request,'news/news_home.html',context={"redirect_url":"/follow/"+topicname+"/"})
    else:
        # [TASK] This function can be greatly improved. Topic model can be changed
        # to accomodate last queried time so that we can query the same query
        # from last time queried till now. For now, this is queried everytime
        # the user queries something
        start_time = time.time()

        query = ''
        if request.method == "GET":
            query = request.GET.get('query')
            if query:
                query = str(query).lower().strip()
            page_no = request.GET.get('page', 1)
            try:
                value = None
                pk = None
                try:
                    # print("Read : ",request.GET.get('page', ''))
                    val_pk_dict = eval(request.GET.get('page', ''))
                    value = val_pk_dict['value'][0]
                    pk = val_pk_dict['pk']
                    
                except serializers.InvalidPage:
                    return Http404()
            except:
                pass

        query = str(topicname).lower().strip()

        # Check if query exists. A query with actual + in it might be present
        if not Topic.objects.filter(name=query).exists():
            query = query.replace("+"," ")
        # usertopics = []
        required_articles = []
        
        if not Topic.objects.filter(name=query).exists():
            if settings.USE_GOOGLE_API:
                required_articles = fetch_articles(query)
            else:
                required_articles = []
                topic, if_created = Topic.objects.get_or_create(name=str(query))

        topic = Topic.objects.filter(name=str(query))
        # try:
        #     topic, if_created = Topic.objects.get_or_create(name=str(query))
        #     if if_created:
        #         if settings.USE_GOOGLE_API: # If the topic doesn't exist fetch newly
        #             required_articles = fetch_articles(query)
        #     else:
        #         required_articles = topic.related_articles.all()
        # except Exception as e:
        #     logger.error(e)

        # Get articles of related topic
        # try:
        #     related_topics = Related_Topic.objects.filter(name=str(query).lower()).prefetch_related("topics__related_articles")
        #     if related_topics:
        #         for related_topic in related_topics[0].topics.all():
        #             required_articles = required_articles | related_topic.related_articles.all()
                
        #         required_articles = required_articles.distinct()
        #         # print(len(required_articles))      

        # except Exception as e:
        #     logger.error(e)
        #     logger.info(query)
        
        # logger.info(f"Time till pagination starts: {time.time() - start_time}")

        if query:
            topic_name = query
            # see if there is a blog with this name
            if topic_name in blog_names or topic_name.startswith("keepup__blog"):
                required_articles = topic[0].related_articles.all()

            else:
                related_topic = Related_Topic.objects.filter(name=topic_name)

                if related_topic:
                    related_topic = related_topic[0]
                    rel_name = related_topic.name
                    q = Q("match_phrase", title=rel_name)
                    related_sub_topics = list(related_topic.topics.values_list('name', flat=True))
                    # all_topics+=related_sub_topics
                    for i_related_sub_topics in related_sub_topics:
                        q = q | Q("match_phrase", title=i_related_sub_topics)
                else:
                    # q = Q("match", title=topic[0].name)
                    words = topic_name.split()
                    q = Q("match", title=words[0])
                    for word in words[1:] :
                        q = q & Q("match", title=word)
                
                required_articles = ArticleDocument.search()[:2000].query(q).sort('-publishedAt')#.to_queryset().order_by("-publishedAt") # topic[0].related_articles.all()
                if required_articles.count()<1 and topic:
                    required_articles = topic[0].related_articles.all()
                # required_topic = TopicDocument.search()[:1].query("match_phrase", name=topic[0].name).extra(size=100)
                # for req_topic in required_topic:
                #     required_articles = req_topic.related_articles
                #     print(len(required_articles))
                #     break
                # required_articles = required_topic[0].related_articles
                # required_articles.execute()
                # s = es_search.query("match", title=topic[0].name)
                # response = s.execute()
                # print(response)
                # for hit in required_articles:
                #     print(hit.publishedAt)

        # Get the topic which might be newly added
        topic, if_created = Topic.objects.get_or_create(name=str(query))

        # See if the user follows the topic. This part is actually not required
        # But it is being added assuming there might be some error where user is
        # following a topic even though it doesn't exist in database
        if topic:
            if request.user.is_anonymous:
                usertopics = []
            else:
                usertopics = UserTopics.objects.filter(user=request.user, topic=topic)


        articles_info_list = paginate(required_articles,page_no, 10)
        # article_topics = [[topic]] * len(required_articles)
        # logger.info(f"Time till pagination: {time.time() - start_time}")
        articles_info_map = {"articles" : []}
        for article in articles_info_list:
            individual_article = dict()
            individual_article["article_obj"] = article
            individual_article["related_topics"] = [topic]
            individual_article["alternate_img"] = get_alternate_image_path(article.url)
            individual_article["alternate_source"] = get_source(article.url)
            articles_info_map["articles"].append(individual_article)
        # logger.info(f"Time till for loop: {time.time() - start_time}")

        messages = []

        if not articles_info_map["articles"]:
            message = NEWS_HOME_USER_NOT_FOLLOWING_MESSAGE.replace("<<topic_name>>",str(topic.name).capitalize())
            messages.append(message)

        
        # Asking the user to follow
        # If the user already follows the topic, disables feature to show the topic and ask him to follow
        if usertopics:
            topic = []
        else :
            topic = [topic]
        redirect_context = {"articles_info_list":articles_info_list, "topics" : topic,
                                                        'messages' : messages, "url":"get_topic_by_query",
                                                        "searched_query":query, "articles_info_map":articles_info_map,
                                                        "free_subscription":1}
        # articles_zip = zip(required_articles, article_topics)
        # logger.info(f"The length: {len(required_articles)}")
        # logger.info(f"Time till the end: {time.time() - start_time}")
        return render(request, 'news/news_home.html',redirect_context)


def concalls_view(request):
    concalls = pd.read_csv(os.path.join(abs_path,"utils/Concalls Today.csv"),encoding="UTF-8")
    time = concalls['Time'].values
    company = concalls['Company'].values
    web_registration = concalls['Web Registration'].values
    phone = concalls['Phone'].values
    object_list = []
    for i, timestamp in enumerate(time):
        temp_dict = {}
        temp_dict["time"] = timestamp
        temp_dict["company"] = company[i]
        temp_dict["web_registration"] = web_registration[i]
        temp_dict["phone"] = phone[i].split("/")
        object_list.append(temp_dict)
    return render(request, 'news/concalls.html',{'objects_list':object_list})


def trending_view(request):
    trending_topics = pd.read_csv(os.path.join(abs_path,"utils/Trending Topics.csv"),encoding="UTF-8")
    time = trending_topics['Display name'].values
    web_registration = trending_topics['Image URL'].values
    company = trending_topics['Search Term'].values
    phone = ["8296458495"]
    object_list = []
    for i, timestamp in enumerate(time):
        temp_dict = {}
        temp_dict["time"] = timestamp
        temp_dict["company"] = company[i]
        temp_dict["web_registration"] = web_registration[i]
        temp_dict["phone"] = phone[i].split("/")
        object_list.append(temp_dict)
    return render(request, 'news/concalls.html',{'objects_list':object_list})

@login_required
def discovery_page(request):
    if request.method=="POST":
        topic_query = str(request.POST.get("topic")).lower().strip()
        try:
            topic, if_created = Topic.objects.get_or_create(name=topic_query)

            usertopic = UserTopics(user=request.user, topic=topic)
            usertopic.save()
        except Exception as e :
            logger.error(e)
    
    user_topics = set(UserTopics.objects.filter(user=request.user).values_list('topic__name', flat=True))
    # sectors = Related_Topic.objects.exclude(name__in=user_topics).all()[:6]
    sectors = [x for x in discover_sectors if x not in user_topics][:6]
    # companies = Company.objects.exclude(company_name__in=user_topics).all()[:6]
    companies =  [x for x in discover_companies if x not in user_topics][:6]
    # topics = Topic.objects.exclude(name__in=user_topics).values('name').annotate(name_count=Count('related_articles')).order_by("-name_count")[11:17]
    topics =  [x for x in discover_topics if x not in user_topics][:6]

    context = {"sectors":sectors,"companies":companies,"topics":topics}
    return render(request, 'news/discovery_page.html', context=context)

@login_required
def discover_each_item(request, item):
    # start_time = time.time()
    if request.method=="POST":
        topic_query = str(request.POST.get("topic")).lower().strip()
        try:
            topic, if_created = Topic.objects.get_or_create(name=topic_query)

            usertopic = UserTopics(user=request.user, topic=topic)
            usertopic.save()
        except Exception as e :
            logger.error(e)
    item = str(item).lower().strip()
    items = []
    field = []
    user_topics = set(UserTopics.objects.filter(user=request.user).values_list('topic__name', flat=True))
    if item=="sectors":
        field = "sectors"
        # items = Related_Topic.objects.exclude(name__in=user_topics).values("name")[:200]
        items = [x for x in discover_sectors if x not in user_topics]
    
    if item=="companies":
        # items = Company.objects.exclude(company_name__in=user_topics).values(name=F('company_name'))[:200]
        items = [x for x in discover_companies if x not in user_topics]
        field = "companies"
    
    if item=="topics":
        # items = Topic.objects.exclude(name__in=user_topics).values('name').annotate(name_count=Count('related_articles')).order_by("-name_count")[:200]
        items = [x for x in discover_topics if x not in user_topics]
        field = "topics"
    
    context = {"items" : items, "field":field}
    # logger.info(f"Time till the end: {time.time() - start_time}")
    return render(request, 'news/discover_each.html', context=context)


@login_required
def follow_topic(request):
    if request.method=="POST":
        topic_query = str(request.POST.get("topic")).lower().strip()
        try:
            topic, if_created = Topic.objects.get_or_create(name=topic_query)

            usertopic, if_created = UserTopics.objects.get_or_create(user=request.user, topic=topic)
            # usertopic.save()
        except Exception as e :
            logger.error(e)
    
    referrer = str(request.META.get('HTTP_REFERER'))

    if referrer:
        if "sectors" in referrer:
            return discover_each_item(request, "sectors")
        
        if "companies" in referrer:
            return discover_each_item(request, "companies")
        
        if "topics" in referrer:
            return discover_each_item(request, "topics")
        
    return redirect("news:discovery_page")

# @user_passes_test(lambda u: u.is_superuser)
@login_required
def analytics_page(request):
    if not request.user.is_superuser:
        return redirect("news:topic_feed")
 
    topic_count = Topic.objects.values('name').annotate(name_count=Count('related_articles'))
    descending_topic =  topic_count.order_by('-name_count')[:10]
    user_topics_count = UserTopics.objects.values('user__email').annotate(topic_count=Count('topic')).order_by('-topic_count')[:10]
    user_following_count = UserTopics.objects.values('topic__name').annotate(topic_name_count=Count('topic__name')).order_by('-topic_name_count')[:10]
    users_count = User.objects.all().count()

    return render(request, 'news/admin_page.html',{"descending_topic" : descending_topic, "users_topics_count":user_topics_count,
                                                            "users_count":users_count, "user_following_count":user_following_count})

@login_required
def autocomplete_home_page_text(request):
    start = time.time()
    if request.is_ajax():
        query = request.GET.get('term')
        query = str(query).lower().strip()
        topics = UserTopics.objects.select_related("topic__name").filter(user=request.user, topic__name__startswith=query).values_list("topic__name",flat=True)
        # topics = [utopic.topic.all().values_list("name") for utopic in user_topics]
        # print(topics)
        sectors = list(Related_Topic.objects.filter(name__istartswith=query).values_list("name",flat=True)[:5])+\
            list(Company.objects.filter(company_name__istartswith=query).values_list("company_name",flat=True)[:5])+\
                list(topics[:5])
        sectors = list(map(str.upper, sectors))
        sectors = list(set(sectors))
        data = json.dumps(sectors)
    else:
        data = 'fail'
    mimetype = 'application/json'
    # logger.info(f"Autocomplete time : {time.time() - start}")
    return HttpResponse(data, mimetype)


def get_all_sources():
    if settings.USE_GOOGLE_API:
        newsapi = NewsApiClient(api_key=settings.GOOGLE_NEWS_API_KEY)
        sources = newsapi.get_sources()
        # skipped = []
    
        if sources['status'].lower() != "ok" :
            # [TASK] Need an error page
            logger.error("Unable to fetch the sources\nError: {}".format(str(sources['message'])))
            return HttpResponseNotFound('<h4>'+ str(sources['message']) +'</h4>')

        for source in sources['sources']:
            source_obj = Source()
            for key, value in source.items():
                if key == 'id':
                    setattr(source_obj,'api_id',value)
                else :
                    setattr(source_obj,key,value)
            
            try:
                source_obj.save()
            except Exception as e:
                logger.exception(e)
    
    else:
        logger.warn("Cannot import sources. Usage of Google API is disabled. Please change in the settings.")



def fetch_top_headlines():
    '''
    Fetched all top headlines of the day by recursively querying 
    till all the articles are fetched. The recursive fetching is
    because the Google News API provides only 20 articles per
    request.

    This function is useful for scheduling.
    '''
    if settings.USE_GOOGLE_API:
        newsapi = NewsApiClient(api_key=settings.GOOGLE_NEWS_API_KEY)
        today = date.today()
        dt_string = today.strftime("%d/%m/%Y")
        query = "top_headlines_"+dt_string
        top_headlines = newsapi.get_top_headlines(language='en', page_size=99)
        
        page_no = 1
        while top_headlines['status'].lower() == "ok" :
            try:
                top_headlines = newsapi.get_top_headlines(language='en', page=page_no)
            except :
                break
            page_no = page_no + 1
            
            add_articles_to_database(top_headlines['articles'], query=str(query).lower().strip())

    else :
        logger.warn("Cannot fetch top headlines. Usage of Google API is disabled. Please change in the settings.")
    
    # return render(request, 'news/test.html',{"skipped" : skipped})

class TopHeadlinesListView(LoginRequiredMixin, ListView):
    # template_name = "news/articles.html"
    template_name = "news/news_home.html"
    context_object_name = 'articles_list'

    def get_queryset(self):
        today = date.today()
        dt_string = today.strftime("%d/%m/%Y")
        query = "top_headlines_" + dt_string
        topic = Topic.objects.filter(name__contains=str(query).lower().strip())
        if not topic :
            if settings.USE_GOOGLE_API:
                newsapi = NewsApiClient(api_key=settings.GOOGLE_NEWS_API_KEY)
                top_headlines = newsapi.get_top_headlines(language='en', page_size=21)
                add_articles_to_database(top_headlines['articles'], query=str(query).lower().strip())
                topic = Topic.objects.filter(name__contains=str(query).lower().strip())
        

        # print(topic[0].related_articles.all())
        if settings.USE_GOOGLE_API:
            return []
        else :
            if topic:
                return topic[0].related_articles.all()
            else:
                return []

class HomeSearchView(LoginRequiredMixin,TemplateView):
    template_name = "news/search_page.html"

# Test url
def test(request):
    logger.info("*"*200)
    wanted = ["Test URL"]

    # from django.db.models import Count
    # # wanted = Topic.objects.annotate(co=Count('related_articles')).order_by("-co")
    # ut = UserTopics.objects.filter(user=request.user).select_related("topic")#.prefetch_related("topic__related_articles")
    # type(ut)
    # topic = Topic.objects.all()[0:1][0]
    # wanted = ut[0].topic.related_articles.all()[:5]
    # print(type(wanted))

    # # Remove repeated topics
    # lastSeenId = "<<>>"
    # rows = Topic.objects.all().order_by('name')

    # for row in rows:
    #     if row.name == lastSeenId:
    #         row.delete() # We've seen this id in a previous row
    #         # wanted.append(row.name)
    #         # print(row.related_articles.all())
    #     else: # New id found, save it and check future rows for duplicates.
    #         lastSeenId = row.name
    
    from .utils.task import TaskImplementor,  DownloadGoogleSheetTask, DownloadGoogleSheetTrendingTopicsTask

    # from .documents import ArticleDocument

    start = time.time()
    # wanted = ArticleDocument.search().query("match", title="ion").to_queryset()
    
    start = time.time()
    implementor = TaskImplementor()
    implementor.register(DownloadGoogleSheetTask())
    implementor.register(DownloadGoogleSheetTrendingTopicsTask())
    implementor.implement()

    # from .utils.utils import add_rss_articles_by_query
    

    logger.info("*"*300)
    logger.info("\n"*5)
    logger.info(f"Scheduling completed.\nTotal time taken for scheduling is {time.time()-start}s.")
    logger.info("\n"*5)
    logger.info("*"*300)

    # related_topic = Related_Topic.objects.get(name="Banks")
    # logger.info(related_topic.topics.all())
    # logger.info(related_topic.topics.all()[0].related_articles.all())


    return render(request, 'news/test.html',{"skipped" : wanted})
