import logging
import re
import time
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http.response import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.db.models import F
from django.urls import reverse
from django.utils.http import urlencode

from ..models import BlogTopic, UserBlogTopics
from ..utils.task import BlogTopicsAdditionTask, TaskImplementor

from elasticsearch_dsl import Q
from ...news.documents import ArticleDocument
from ..utils.pipelines import BlogUpdatePipeline

logger = logging.getLogger(__name__)


ARTICLES_PER_PAGE = 7

## TODO Change all follow requests from GET to POST

@login_required
def blogs_news_page(request):
    return render(request,"news/blogs_home.html",{})

@login_required
def blogs_follow_topic(request):
    res = {"data":{
        "blog_follow_status" : "Not_Successful",
        "blog_name" : ""
    }}
    if request.method == "GET":
        blog_to_be_followed = request.GET.get("blog").strip()
        try:
            blog_topic_instance = BlogTopic.objects.get(name=blog_to_be_followed)
            user_blog_instance, is_created = UserBlogTopics.objects.get_or_create(user=request.user, blog_topic=blog_topic_instance)
            res = {"data" : {
                "blog_follow_status" : "Successful",
                "blog_name" : blog_topic_instance.name
            }}
            return JsonResponse(res)
        except Exception as e:
            logger.error(e)
            return JsonResponse(res)
    return JsonResponse(res)


@login_required
def blogs_unfollow_topic(request):
    res = {"data":{
        "blog_unfollow_status" : "Not_Successful",
        "blog_name" : ""
    }}
    if request.method == "GET":
        blog_to_be_followed = request.GET.get("blog").strip()
        try:
            blog_topic_qset = BlogTopic.objects.filter(name=blog_to_be_followed)
            if not blog_topic_qset:
                res["data"]["blog_unfollow_status"] = "Topic not found"
                return JsonResponse(res)
            user_blog_qset = UserBlogTopics.objects.filter(user=request.user, blog_topic=blog_topic_qset[0])
            
            if not user_blog_qset :
                res["data"]["blog_unfollow_status"] = "Already not following the topic"
                return JsonResponse(res)
            
            user_blog_qset.delete()
            res = {"data" : {
                "blog_unfollow_status" : "Successful",
                "blog_name" : blog_topic_qset[0].name
            }}
            return JsonResponse(res)
        except Exception as e:
            logger.error(e)
            return JsonResponse(res)
    return JsonResponse(res)


@login_required
def blogs_autocomplete_topic(request):
    if request.method == "GET":
        try:
            substr = request.GET.get('search').strip()
            if not substr:
                return JsonResponse({"data":""})
        except Exception as e:
            logger.error(e)
            return JsonResponse({"data":""})

        blog_topics = list(BlogTopic.objects.filter(display_name__icontains=substr).values('display_name','name'))
        
        return JsonResponse({"data":blog_topics})
    
    return JsonResponse({"data":[]})


@login_required
def blogs_get_all_user_following_topics(request):
    res = {"data":{
        "blog_topics_following_status" : "Not_Successful",
        "blog_topics_following" : []
    }}
    if request.method == "GET":
        try:
            user_blog_following_instance = UserBlogTopics.objects.filter(user=request.user)
            user_blog_following_queryset = user_blog_following_instance.values(display_name=F("blog_topic__display_name"),name=F("blog_topic__name"))
            # print(user_blog_following_queryset)
            res = {"data":{
                "blog_topics_following_status" : "Successful",
                "blog_topics_following" : list(user_blog_following_queryset)
            }}
            return JsonResponse(res)
        except Exception as e:
            logger.error(e)
            return JsonResponse(res)
    return JsonResponse(res)


def paginate(object_list, page_no, default_num_per_page = 20):
    paginator = Paginator(object_list, default_num_per_page)
    try:
        objects = paginator.page(page_no)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    
    return objects

@login_required
def blogs_fetch_news_articles(request):
    retval = {"data":[]}
    if request.method == "GET":
        page_no = request.GET.get('page', 1)

    user_blog_topics = list(UserBlogTopics.objects.filter(user=request.user).values_list("blog_topic__name",flat=True))
    i = 0
    q = None
    for user_blog_topic in user_blog_topics:
        if i==0:
            q = Q("match_phrase", tags=user_blog_topic)
        else :
            q = q | Q("match_phrase", tags=user_blog_topic)
        i = i + 1

    if q :
        required_articles = ArticleDocument.search()[:i*1000].query(q).sort('-publishedAt')
    else :
        required_articles = []
    # for a in required_articles[:7]:
    #     print(a.title, a.description)
    # TODO For all the page numbers above the total pages actually present, the 
    #      paginator gives the last page. Thus the UI might show repeated articles 
    #      once it reaches the end. Need to fix this
    paginate_article_list = paginate(required_articles, page_no, ARTICLES_PER_PAGE)

    # Note : In artile tags first tag should be blog topic
    articles_info_list = []
    for article in paginate_article_list:
        individual_article = {}
        individual_article["title"] = article.title
        individual_article["tags"] = article.tags
        if article.source and article.source.name :
            individual_article["source"] = article.source.name
        else :
            individual_article["source"] = ""
        # individual_article["author"] = article.author # Not indexed
        individual_article["url"] = article.url
        individual_article["urlToImage"] = article.urlToImage
        individual_article["disseminated_time"] = article.publishedAt
        # individual_article["content"] = article.content # Not indexed
        all_topics = []
        article_blog_topics = article["tags"].lower().split(" <space> ")
        all_topics.extend(article_blog_topics)
        individual_article["topic_urls"] = []
        individual_article["topic_names"] = []
        if article_blog_topics:
            individual_article["related_topics"] = list(BlogTopic.objects.filter(name=article_blog_topics[0]).values_list("display_name",flat=True))
            
        else :
            individual_article["related_topics"] = []
        
        for topic_name in all_topics:
            # print(reverse("news:get_everything_by_query")+"?"+urlencode({"query":topic_name}))
            individual_article["topic_urls"].append(reverse("news:get_everything_by_query")+"?"+urlencode({"query":topic_name}))
            individual_article["topic_names"].append(topic_name)
        individual_article["alternate_img"] = "" # TODO get_alternate_image_path(str(article.url))
        individual_article["alternate_source"] = "" # TODO get_source(str(article.url))
        retval["data"].append(individual_article)
    
    return JsonResponse(retval)


def blogs_test(request):
    # BlogUpdatePipeline().run()
    start = time.time()
    implementor = TaskImplementor()
    implementor.register(BlogTopicsAdditionTask())
    implementor.implement()
    wanted = ["Done"]

    return render(request, 'news/test.html',{"skipped" : wanted})

