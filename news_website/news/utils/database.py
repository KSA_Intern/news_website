import logging

from ..models import Topic, Article, Source, UserTopics, SecondaryArticle
from ..nlp.processor import SentenceProcessor
from .processing import Preprocessor

logger = logging.getLogger(__name__)



class DatabaseFurnisher(object):
    """
    All the data added to the database is be
    done by this class.
    """

    def __init__(self):
        """
        docstring
        """
        self.sentence_processor = SentenceProcessor()
        self.preprocessor = Preprocessor()

    def add_articles_to_secondary_db(self,news_articles,model,topic = None, google_rss = False, tags = []):
        for article in news_articles:

            sentence_keywords = self.sentence_processor.filter_named_entities(article['title'])

            # Ignoring the article if it has no topics to store it under
            # if not sentence_keywords:
            #     continue

            article = self.preprocessor.preprocess_article(article, google_rss, tags)
        
            try:
                model_instance, if_created = model.objects.get_or_create(
                description=article['description'],
                url=article['link'],
                source=article["source"],
                author=article['author'],
                title=article['title'],
                urlToImage=article['image'],
                publishedAt=article['pubDate'],
                content=article['content'],
                tags=article["tags"]
                )
            except Exception as identifier:
                logger.info(identifier)
                continue
                # article_instance = Article.objects.filter(url=article['link'])
                # if model_instance:
                #     model_instance = article_instance[0]
            
            try:
                model_instance.save()
            except Exception as identifier:
                logger.info(identifier)
                logger.info('Unable to save the article with title {}'.format(article['title']))

            keywords_string = ""
            for topic_keywords in sentence_keywords:
                keywords_string = keywords_string + "<<SPACE>>" + topic_keywords

            if model_instance:
                model_instance.keywords = keywords_string
            
            if topic:
                if  model_instance:
                    model_instance.topic = topic
            
            try:
                model_instance.save()
            except Exception as identifier:
                logger.info(identifier)
                logger.info('Unable to save the article with title {}'.format(article['title']))


    def check_secondary_db(self, title):
        try:
            article = SecondaryArticle.objects.get(title=title)
        except Exception as e:
            logger.debug("Error: "+ str(e))
            article = SecondaryArticle.objects.filter(title=title)
            if article:
                article = article[0]
        
        if article:
            logger.info("Found in secondary DB and deleting - " + title)
            article.delete()

    
    def add_articles_to_db(self,news_articles, topic=None, google_rss=False, add_article_to_topic=False, tags=[]):
        '''
        Adds articles to database with relevant topics.
        This function expects news_articles list to be of
        certain structure. Each dictionary item can have 
        ['title','link','image','guid','pubDate','description','author','source']
        keys
        
        Args:
        news_articles: News articles to add
        topic : Topic under which news_articles are added
        google_rss : If the articles fetched are from google_rss, then this arg will be true
        add_article_to_topic : If this is set to true, then the article is added to the topic
                               only if the topic exists in the name. Usually used for google_news
                               or company news fetching
        tags : Contains the tags to be added along with article
        '''
        for article in news_articles:

            sentence_keywords = self.sentence_processor.filter_named_entities(article['title'])

            # Ignoring the article if it has no topics to store it under
            if not sentence_keywords and not topic:
                logger.warn("[IMPORTANT] Skipping article because of no topic given")
                continue

            article = self.preprocessor.preprocess_article(article, google_rss, tags)
            try:
                fetched_source, if_created = Source.objects.get_or_create(name=article["source"])
            except Exception as e:
                logger.info("Error : ", str(e))
                fetched_source, if_created = Source.objects.get_or_create(name="")
            
            # logger.info("Source is "+article['source'])

            try:
                article_instance, if_created = Article.objects.get_or_create(
                description=article['description'],
                url=article['link'],
                source = fetched_source,
                author=article['author'],
                title=article['title'],
                urlToImage=article['image'],
                publishedAt=article['pubDate'],
                content=article['content'],
                tags=article["tags"]
                )
            except Exception as identifier:
                logger.info(identifier)
                logger.info("Skipping the article "+article["title"])
                # print(article)
                continue
                # article_instance = Article.objects.filter(url=article['link'])
                # if article_instance:
                #     article_instance = article_instance[0]
            
            try:
                article_instance.save()
            except Exception as identifier:
                logger.info(identifier)
                logger.info('Unable to save the article with title {}'.format(article['title']))
            
            if article_instance:
                self.check_secondary_db(article_instance.title)
            else :
                continue

            if topic:
                try:
                    topic_instance, if_created = Topic.objects.get_or_create(name=str(topic))
                    if if_created:
                        logger.info("Created new feed topic {}".format(topic))
                    else :  
                        # logger.info("{} topic already exists".format(topic))
                        pass
                except Exception as identifier:
                    logger.info(identifier)
                    topic_instance = Topic.objects.filter(name=str(topic))
                    logger.info("The topics for keyword "+topic+"is - ")
                    logger.info(topic_instance)
                    topic_instance = topic_instance[0]
                
                if google_rss or add_article_to_topic:
                    if str(topic).lower() in article_instance.title.lower():
                        # logger.info("Article {} added under topic {}".format(article["title"],topic))
                        topic_instance.related_articles.add(article_instance)
                else:
                    # logger.info("Article {} added under topic {}".format(article["title"],topic))
                    topic_instance.related_articles.add(article_instance)
                
                topic_instance.save()

            for keyword_topic in sentence_keywords :
                keyword_topic = str(keyword_topic).lower()
                try:
                    topic_instance, if_created = Topic.objects.get_or_create(name=str(keyword_topic))
                    if if_created:
                        logger.info("Created new topic {}".format(keyword_topic))
                except Exception as identifier:
                    logger.info(identifier)
                    topic_instance = Topic.objects.filter(name=str(keyword_topic))
                    logger.info("The topics for keyword "+keyword_topic+"is - ")
                    logger.info(topic_instance)
                    topic_instance = topic_instance[0]
                
                topic_instance.related_articles.add(article_instance)
                topic_instance.save()