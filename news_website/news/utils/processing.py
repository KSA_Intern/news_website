import pandas as pd
import re
import os
import logging

logger = logging.getLogger(__name__)

abs_path = os.path.dirname(os.path.abspath(__file__))
url_image_data = pd.read_csv(os.path.join(abs_path,"Sources to show from google rss - Sheet1.csv"))

# This function is also process in RSSFeedProcessor and this might cause a lot
# of problems. So TODO task is to make this function a separate class and call it
# using that class
def get_source(url):
    if "economictimes" in url:
        return "Economic Times"
    elif "bloombergquint" in url:
        return "Bloomberg Quint"
    elif "livemint" in url:
        return "Livemint"
    # elif "financialexpress" in url:
    #     return "Financial Express"
    elif "businesstoday" in url:
        return "Business Today"
    elif "forbesindia" in url:
        return "Forbes India"
    elif "wionews" in url:
        return "Wion"
    elif "capitalmind" in url:
        return "Capitalmind"
    elif "finshots" in url:
        return "Finshots"
    elif "safalniveshak" in url:
        return "Safal Niveshak"
    elif "indiaspend" in url:
        return "India Spend"
    else :
        return ""

class Preprocessor(object):
    """
    Article Preprocessor
    """

    def month_to_num(self, month):
        month_mapper = {
            "Jan" : "01",
            "Feb" : "02",
            "Mar" : "03",
            "Apr" : "04",
            "May" : "05",
            "Jun" : "06",
            "Jul" : "07",
            "Aug" : "08",
            "Sep" : "09",
            "Oct" : "10",
            "Nov" : "11",
            "Dec" : "12",
        }
        if month in month_mapper:
            return month_mapper[month]
        else :
            return "00"

    def preprocess_article(self, article, google_rss=False, tags=[]):
        # Add attributes if not present
        attributes = ['title','link','image','guid','pubDate','description','author','source','tags']
        for attribute in attributes:
            if not attribute in article:
                article[attribute] = ""
        # This is to remove html tags from title
        if "<" in article['title'] and ">" in article['title']:
            temp = article['title']
            temp = re.sub(r'<[^\n]*?>', '', temp)
            article['title'] = temp
        # This is to pre process livemint feeds
        if article['pubDate'][:2].isalpha():
            date_unformatted = article['pubDate']
            date_formatted = date_unformatted[12:16] + '-' + self.month_to_num(date_unformatted[8:11]) + '-' + \
                date_unformatted[5:7] + 'T' + date_unformatted[17:]
            article['pubDate'] = date_formatted            
        # This is to preprocess moneycontrol rss feeds
        if not article['image'] and "<img" in article['description']:
            temp = re.findall(r'src="[^\n]*?"', article['description'])[0]
            article['image'] = re.findall(r'"[^\n]*?"',temp)[0][1:-1]
            article['description'] = re.sub(r'<[^\n]*?>', '', article['description'])
        
        article['description'] = re.sub(r'(\n)|(\t)|(&nbsp)|;', '', article['description'])
        article['description'] = re.sub(r'<[^\n]*?>', '', article['description'])
        # This is for some of ET rss feeds
        if '<a' in article['description']:
            article['description'] = re.sub(r'<a.*?</a>', '',article['description'])
        
        if not 'content' in article or not article["content"]:
            article['content'] = str(article['description'])[:230] + '...'
        else :
            article['content'] = re.sub(r'(\n)|(\t)|(&nbsp)|;', '', article['content'])
            article['content'] = re.sub(r'<[^\n]*?>', '', article['content'])
        
        if not article['description']:
            article['description'] = article['content']
            article['content'] = article['content'][:230]
        
        # This is for Google RSS feeds
        if google_rss:
            if not article["source"]:
                split_list = article["title"].split("-")
                article["source"] =  split_list[-1].strip() if  len(split_list)>1 else ""
                article["title"] = "-".join(split_list[:-1]).strip()
        
        if not article['source']:
            article['source'] = get_source(article['link'])
        
        if tags:
            article["tags"] = ' <end_of_topic> '.join(tags)

        return article

    def found_in_allowed_urls(self, url, return_data_row = False):
        for index in range(len(url_image_data)):
            if (pd.isnull(url_image_data.loc[index,"URL"])):
                continue
            processed_url = re.sub(r'(https|http)*:*(//)*(www.)*',"",url_image_data.loc[index,"URL"])[:-1]
            if processed_url in url:
                if return_data_row:
                    return True, url_image_data.loc[index]
                else :
                    return True
        if return_data_row:
            return False, pd.Series({"URL":"", "Image":""})
        else :
            return False
    
    def process_company_names(self, name):
        """
        docstring
        """
        