from ..models import Topic, Article, Source, UserTopics, SecondaryArticle, Related_Topic
import requests 
import xml.etree.ElementTree as ET
import os
import re
import pandas as pd
import logging

from django.conf import settings

from .rss_feeds import get_rss_list, get_blogs_rss_list

logger = logging.getLogger(__name__)

TRACKED_SOURCES = []

abs_path = os.path.dirname(os.path.abspath(__file__))
url_image_data = pd.read_csv(os.path.join(abs_path,"Sources to show from google rss - Sheet1.csv"))

################## NLP Part ################
import spacy

try:
    nlp = spacy.load("en_core_web_sm")

except Exception as e:
    os.system("python -m spacy download en_core_web_sm")
    nlp = spacy.load("en_core_web_sm")

# https://spacy.io/api/annotation#named-entities
ACCEPTED_LABELS_AS_TOPICS = ['PERSON', 'NORP', 'FAC', 'ORG', 'GPE', 'LOC', 'PRODUCT', 'EVENT','WORK_OF_ART', 'LAW',
                                'PER', 'MISC']

ACCEPTED_POS_AS_TOPICS = ['PROPN']

def filter_named_entities(sentence):
    '''
        Filters named entities from a sentence and returns
        tokens of words that are in accepted set
    '''

    doc = nlp(sentence)

    filtered_sentence = []
    
    for ent in doc.ents:
        if ent.label_ in ACCEPTED_LABELS_AS_TOPICS:
            filtered_sentence.append(ent.text)
    
    for token in doc:
        if token.pos_ in ACCEPTED_POS_AS_TOPICS and not token.text in filtered_sentence:
            filtered_sentence.append(token.text)
    
    return filtered_sentence


############################################

# Add only those sources which you are following.
# Because google news rss uses this function to
# find out if the news article is already a part
# of our existing following source        
def get_source(url):
    if "economictimes" in url:
        return "Economic Times"
    elif "bloombergquint" in url:
        return "Bloomberg Quint"
    elif "livemint" in url:
        return "Livemint"
    # elif "financialexpress" in url:
    #     return "Financial Express"
    elif "businesstoday" in url:
        return "Business Today"
    elif "forbesindia" in url:
        return "Forbes India"
    elif "wionews" in url:
        return "Wion"
    elif "capitalmind" in url:
        return "Capitalmind"
    elif "finshots" in url:
        return "Finshots"
    elif "safalniveshak" in url:
        return "Safal Niveshak"
    elif "indiaspend" in url:
        return "India Spend"
    else :
        return ""

def found_in_allowed_urls(url, return_data_row = False):
    for index in range(len(url_image_data)):
        if (pd.isnull(url_image_data.loc[index,"URL"])):
            continue
        processed_url = re.sub(r'(https|http)*:*(//)*(www.)*',"",url_image_data.loc[index,"URL"])[:-1]
        if processed_url in url:
            if return_data_row:
                return True, url_image_data.loc[index]
            else :
                return True
    if return_data_row:
        return False, pd.Series({"URL":"", "Image":""})
    else :
        return False

def get_alternate_image_path(url):
    if "economictimes" in url:
        return os.path.join(settings.STATIC_URL,"images/news/economictimes_1.jpeg")
    elif "bloombergquint" in url:
        return os.path.join(settings.STATIC_URL,"images/news/bloombergquint_1.jpg")
    elif "businesstoday" in url:
        return os.path.join(settings.STATIC_URL,"images/news/businesstoday_1.jpeg")
    elif "wionews" in url:
        return "https://cdn.wionews.com/images/WION-logo-1200.png"
    elif "capitalmind" in url:
        return os.path.join(settings.STATIC_URL,"images/news/capitalmind_logo.png")
    elif "safalniveshak" in url:
        return os.path.join(settings.STATIC_URL,"images/news/safalniveshak_1.jpeg")
    elif "livemint" in url:
        return os.path.join(settings.STATIC_URL,"images/news/livemint.jpeg")
    else :
        allowed, data_row = found_in_allowed_urls(url, return_data_row=True)
        if allowed:
            return data_row["Image"]
        else :
            return ""

def add_articles_to_database(articles_list, query=None):
    '''
        Given the articles list fetched from Google API
        adds all the articles to the database. If query 
        is not None, then the articles are put under topic
        named by query.
    '''

    skipped = []
    topic = Topic.objects.filter(name=str(query)).distinct()

    if topic:
        topic = topic[0]

    if not articles_list:
        logger.info("Articles list fetched for query {} is empty.".format(query))
    # logger.debug("Articles list is {}".format(articles_list))
    for article in articles_list:
        article_inst = Article.objects.filter(url=article['url'])
        if article_inst:
            # logger.info("Article already found")
            article_obj = article_inst[0]
            if query:
                if not topic:
                    topic = Topic.objects.create(name=str(query))
                    topic.related_articles.add(article_obj)
                    topic.save()
                    logger.info("New topic created : "+topic.name)
                else :
                    topic.related_articles.add(article_obj)
                    topic.save()
            continue

        article_obj = Article()
        for key, value in article.items():
            if key == 'source':
                if value['id']:
                    source = Source.objects.filter(api_id=value['id'])
                    # [TASK] Need to handle the case for a new undefined source
                    if source:
                        setattr(article_obj,'source', source[0])
                elif value['name']:
                    source = Source.objects.filter(name=value['name'])
                    if source:
                        setattr(article_obj,'source', source[0])
                else:
                    setattr(article_obj,'source',None)
            else :
                setattr(article_obj,key,value)
        
        try:
            article_obj.save()
            # logger.debug(topic)
            if query:
                if not topic:
                    topic = Topic.objects.create(name=str(query))
                    topic.related_articles.add(article_obj)
                    topic.save()
                    logger.info("New topic created : "+topic.name)
                else :
                    topic.related_articles.add(article_obj)
                    topic.save()


        except Exception as e:
            logger.info("There was an error while saving the topic")
            logger.info(e)
            if query:
                article_obj = Article.objects.get(url=article['url'])
                if article_obj:
                    logger.info("New article is fetched successfully")
                if not topic:
                    topic = Topic.objects.create(name=str(query))
                    
                    topic.related_articles.add(article_obj)
                    topic.save()
                    logger.info("New topic created : "+topic.name)
                else :
                    topic.related_articles.add(article_obj)
                    topic.save()
            # skipped.append(article)


def get_rss_news(url='https://economictimes.indiatimes.com/rssfeedsdefault.cms', 
                 main_tag='item',attr_list=['title','link','image','guid','pubDate','description'],
                 match_list = ['title','link','image','guid','pubDate','description']):
    '''
    Fetches rss feed from given url. 
    How to use ? 
     Pass the url of the rss feed as `url` attribute. Pass the name of the XML tag which contains
     information about the news article as `main_tag` attribute. Pass the list of the attributes
     or the tags that are children of main_tag that is to be fetched as `attr_list` attribute.
     `match_list` conains sequence of attributes - ['title','link','image','guid','pubDate','description']-
     in order that matches the attr_list. i.e if the above list is match_list, then attr_list[0]
     contains the corresponding attribute that fetches info related to match_list[0] - in this case it's 
     title. So attr_list[0] has attribute which fetches title

     The function returns list of dictionary where each entry is an article
    '''
    try:
        response = requests.get(url, headers= {"user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"})
        if response.status_code == 404:
            return []
    except Exception as e:
        logger.info("Unable to fetch this url")
        logger.error(e)
        return []
    try:
        if response.text:
            root = ET.fromstring(response.text)
        else:
            return []
    except Exception as e:
        logger.info("Unable to parse the URL")
        logger.error(e)
        return []
    articles = []
    try:
        items = root.iter(str(main_tag))
    except Exception as e:
        logger.info(e)
        items = []
    for item in items:
        article = {}
        for index, attr in enumerate(attr_list):
            try:
                result = item.find(attr)
                if result.text:
                    article[match_list[index]] = result.text
                elif result.attrib['url'] :
                    article[match_list[index]] = result.attrib['url']
                else :
                    article[match_list[index]] = ''
            except :
                article[match_list[index]] = ''
        articles.append(article)
    return articles

def check_secondary_db(title):
    # logger.info("Checking secondary DB")
    try:
        article = SecondaryArticle.objects.get(title=title)
    except Exception as e:
        logger.debug("Error: "+ str(e))
        article = SecondaryArticle.objects.filter(title=title)
        if article:
            article = article[0]
    
    if article:
        logger.info("Found in secondary DB and deleting - " + title)
        article.delete()

def month_to_num(month):
    month_mapper = {
        "Jan" : "01",
        "Feb" : "02",
        "Mar" : "03",
        "Apr" : "04",
        "May" : "05",
        "Jun" : "06",
        "Jul" : "07",
        "Aug" : "08",
        "Sep" : "09",
        "Oct" : "10",
        "Nov" : "11",
        "Dec" : "12",
    }
    if month in month_mapper:
        return month_mapper[month]
    else :
        return "00"

def preprocess_article(article, google_rss=False):
    # Add attributes if not present
    attributes = ['title','link','image','guid','pubDate','description','author','source']
    for attribute in attributes:
        if not attribute in article:
            article[attribute] = ""
    # This is to remove html tags from title
    if "<" in article['title'] and ">" in article['title']:
        temp = article['title']
        temp = re.sub(r'<[^\n]*?>', '', temp)
        article['title'] = temp
    # This is to pre process livemint feeds
    if article['pubDate'][:2].isalpha():
        date_unformatted = article['pubDate']
        date_formatted = date_unformatted[12:16] + '-' + month_to_num(date_unformatted[8:11]) + '-' + \
            date_unformatted[5:7] + 'T' + date_unformatted[17:]
        article['pubDate'] = date_formatted            
    # This is to preprocess moneycontrol rss feeds
    if not article['image'] and "<img" in article['description']:
        temp = re.findall(r'src="[^\n]*?"', article['description'])[0]
        article['image'] = re.findall(r'"[^\n]*?"',temp)[0][1:-1]
        article['description'] = re.sub(r'<[^\n]*?>', '', article['description'])
    
    article['description'] = re.sub(r'(\n)|(\t)|(&nbsp)|;', '', article['description'])
    article['description'] = re.sub(r'<[^\n]*?>', '', article['description'])
    # This is for some of ET rss feeds
    if '<a' in article['description']:
        article['description'] = re.sub(r'<a.*?</a>', '',article['description'])
    
    if not 'content' in article or not article["content"]:
        article['content'] = str(article['description'])[:230] + '...'
    else :
        article['content'] = re.sub(r'(\n)|(\t)|(&nbsp)|;', '', article['content'])
        article['content'] = re.sub(r'<[^\n]*?>', '', article['content'])
    
    if not article['description']:
        article['description'] = article['content']
        article['content'] = article['content'][:230]
    
    # This is for Google RSS feeds
    if google_rss:
        if not article["source"]:
            split_list = article["title"].split("-")
            article["source"] =  split_list[-1].strip() if  len(split_list)>1 else ""
            article["title"] = "-".join(split_list[:-1]).strip()
    
    if not article['source']:
        article['source'] = get_source(article['link'])

    return article


def add_articles_to_db(news_articles, topic=None, google_rss=False):

    '''
        Adds articles to database with relevant topics.
        This function expects news_articles list to be of
        certain structure. Each dictionary item should have 
        ['title','link','image','pubDate','description']
        keys
    '''

    for article in news_articles:

        sentence_keywords = filter_named_entities(article['title'])

        # Ignoring the article if it has no topics to store it under
        if not sentence_keywords and not topic:
            continue

        article = preprocess_article(article, google_rss)
        try:
            fetched_source, if_created = Source.objects.get_or_create(name=article["source"])
        except Exception as e:
            logger.info("Error : ", str(e))
            fetched_source, if_created = Source.objects.get_or_create(name=article[""])
        
        # logger.info("Source is "+article['source'])

        try:
            article_instance, if_created = Article.objects.get_or_create(
            description=article['description'],
            url=article['link'],
            source = fetched_source,
            author=article['author'],
            title=article['title'],
            urlToImage=article['image'],
            publishedAt=article['pubDate'],
            content=article['content']
            )
        except Exception as identifier:
            logger.info(identifier)
            logger.info("Skipping the article "+article["title"])
            # print(article)
            continue
            # article_instance = Article.objects.filter(url=article['link'])
            # if article_instance:
            #     article_instance = article_instance[0]
        
        try:
            article_instance.save()
        except Exception as identifier:
            logger.info(identifier)
            logger.info('Unable to save the article with title {}'.format(article['title']))
        
        if article_instance:
            check_secondary_db(article_instance.title)
        else :
            continue

        if topic:
            try:
                topic_instance, if_created = Topic.objects.get_or_create(name=str(topic))
                if if_created:
                    logger.info("Created new feed topic {}".format(topic))
                else :  
                    # logger.info("{} topic already exists".format(topic))
                    pass
            except Exception as identifier:
                logger.info(identifier)
                topic_instance = Topic.objects.filter(name=str(topic))
                logger.info("The topics for keyword "+topic+"is - ")
                logger.info(topic_instance)
                topic_instance = topic_instance[0]
            
            if google_rss :
                if str(topic).lower() in article_instance.title.lower():
                    topic_instance.related_articles.add(article_instance)
            else:
                topic_instance.related_articles.add(article_instance)
            
            topic_instance.save()

        for keyword_topic in sentence_keywords :
            keyword_topic = str(keyword_topic).lower()
            try:
                topic_instance, if_created = Topic.objects.get_or_create(name=str(keyword_topic))
                if if_created:
                    logger.info("Created new topic {}".format(keyword_topic))
            except Exception as identifier:
                logger.info(identifier)
                topic_instance = Topic.objects.filter(name=str(keyword_topic))
                logger.info("The topics for keyword "+keyword_topic+"is - ")
                logger.info(topic_instance)
                topic_instance = topic_instance[0]
            
            topic_instance.related_articles.add(article_instance)
            topic_instance.save()

def add_articles_to_secondary_db(news_articles,model,topic = None, google_rss = False):
    for article in news_articles:

        sentence_keywords = filter_named_entities(article['title'])

        # Ignoring the article if it has no topics to store it under
        # if not sentence_keywords:
        #     continue

        article = preprocess_article(article, google_rss)
    
        try:
            model_instance, if_created = model.objects.get_or_create(
            description=article['description'],
            url=article['link'],
            source=article["source"],
            author=article['author'],
            title=article['title'],
            urlToImage=article['image'],
            publishedAt=article['pubDate'],
            content=article['content']
            )
        except Exception as identifier:
            logger.info(identifier)
            continue
            # article_instance = Article.objects.filter(url=article['link'])
            # if model_instance:
            #     model_instance = article_instance[0]
        
        try:
            model_instance.save()
        except Exception as identifier:
            logger.info(identifier)
            logger.info('Unable to save the article with title {}'.format(article['title']))

        keywords_string = ""
        for topic_keywords in sentence_keywords:
            keywords_string = keywords_string + "<<SPACE>>" + topic_keywords

        if model_instance:
            model_instance.keywords = keywords_string
        
        if topic:
            if  model_instance:
                model_instance.topic = topic
        
        try:
            model_instance.save()
        except Exception as identifier:
            logger.info(identifier)
            logger.info('Unable to save the article with title {}'.format(article['title']))


def add_rss_articles_to_db():
    
    rss_list = get_rss_list()
    for rss_item in rss_list:
        logger.info("Starting periodic fetch from "+rss_item['url'])
        news_articles = get_rss_news(rss_item['url'], rss_item['main_tag'], rss_item['attr_list'], rss_item['match_list'])
        if news_articles:
            add_articles_to_db(news_articles, topic=rss_item['topic'])


def add_rss_articles_by_query():
    query_rss = [
        {"url" : "https://news.google.com/rss/search?q=<<query>>&hl=en-IN&gl=IN&ceid=IN:en",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "google_news"},
    ]

    topic_set = set()
    for user_topic in UserTopics.objects.all():
        topic_set.add(user_topic.topic.name)
        
        # try:
        #     related_topic = Related_Topic.objects.get(name=user_topic.topic.name)
        #     for rel_topic in related_topic.topics.all():
        #         topic_set.add(rel_topic.name)
        # except Exception as e:
        #     logger.info(e)
    
    for i_topic in topic_set:
        logger.info("Fetching news for query "+i_topic)
        
        for rss_item in query_rss:
            url = str(rss_item['url']).replace("<<query>>",str(i_topic))
            logger.info("Getting news from "+url)
            news_articles = get_rss_news(url, rss_item['main_tag'], rss_item['attr_list'], rss_item['match_list'])
            if news_articles:
                for news_article in news_articles:
                    sour = get_source(news_article["link"])
                    if sour:
                        if rss_item["topic"] == "google_news":
                            add_articles_to_secondary_db([news_article],SecondaryArticle,topic = str(i_topic), google_rss=True)
                        else :
                            add_articles_to_secondary_db([news_article],SecondaryArticle,topic = str(i_topic))
                        
                        logger.debug("URL has "+sour+" Adding to secondary db")
                    else:
                        if found_in_allowed_urls(news_article["link"]):
                            if rss_item["topic"] == "google_news":
                                add_articles_to_db([news_article], topic=str(i_topic), google_rss=True)
                            else:
                                add_articles_to_db([news_article], topic=str(i_topic))
                            logger.debug("Added to Primary DB - URL was "+news_article["link"])