def get_rss_list():
    rss_list = [
        # ET
        
        {"url" : "https://economictimes.indiatimes.com/rssfeedsdefault.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_et-home"},

        {"url" : "https://economictimes.indiatimes.com/rssfeedstopstories.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_top-stories"},

        {"url" : "https://economictimes.indiatimes.com/prime/rssfeeds/69891145.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_et-prime"},

        {"url" : "https://economictimes.indiatimes.com/budget-2019/rssfeeds/67173653.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_budget-2019"},

        {"url" : "https://economictimes.indiatimes.com/budget/rssfeeds/73067372.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_budget-2020"},

        {"url" : "https://economictimes.indiatimes.com/markets/rssfeeds/1977021501.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_markets"},
        
        {"url" : "https://economictimes.indiatimes.com/news/rssfeeds/1715249553.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/industry/rssfeeds/13352306.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/small-biz/rssfeeds/5575607.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/wealth/rssfeeds/837555174.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/mf/rssfeeds/359241701.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mf"},

        {"url" : "https://economictimes.indiatimes.com/tech/rssfeeds/13357270.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_technology"},

        {"url" : "https://economictimes.indiatimes.com/jobs/rssfeeds/107115.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/opinion/rssfeeds/897228639.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/nri/rssfeeds/7771250.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/magazines/rssfeeds/1466318837.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/multimedia/rssfeeds/68004546.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/technology-and-startups/rssfeeds/63319172.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/consumer/rssfeeds/60187420.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/money-and-markets/rssfeeds/62511286.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/corporate-governance/rssfeeds/63329541.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/media-and-communications/rssfeeds/60187277.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/transportation/rssfeeds/60187459.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},
        
        {"url" : "https://economictimes.indiatimes.com/prime/pharma-and-healthcare/rssfeeds/60187434.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/fintech-and-bfsi/rssfeeds/60187373.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/economy-and-policy/rssfeeds/63884214.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/infrastructure/rssfeeds/64403500.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        
        {"url" : "https://economictimes.indiatimes.com/prime/environment/rssfeeds/63319186.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/prime/energy/rssfeeds/60187444.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},
        
        # ET Markets
        {"url" : "https://economictimes.indiatimes.com/markets/stocks/rssfeeds/2146842.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/markets/ipos/fpos/rssfeeds/14655708.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_ipos"},

        {"url" : "https://economictimes.indiatimes.com/markets/market-moguls/rssfeeds/54953131.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/markets/expert-view/rssfeeds/50649960.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/markets/commodities/rssfeeds/1808152121.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/markets/forex/rssfeeds/1150221130.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/markets/bonds/rssfeeds/2146846.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_bonds"},

        {"url" : "https://economictimes.indiatimes.com/markets/webinars/rssfeeds/53555278.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/rsssymbolfeeds/commodityname-Gold.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_gold"},

        {"url" : "https://economictimes.indiatimes.com/markets/stocks/rssfeeds/53613060.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        # ET news
        {"url" : "https://economictimes.indiatimes.com/news/podcasts/rssfeeds/66647137.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},
        
        {"url" : "https://economictimes.indiatimes.com/news/newsblogs/rssfeeds/65098458.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/economy/rssfeeds/1373380680.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_economy"},

        {"url" : "https://economictimes.indiatimes.com/news/politics-and-nation/rssfeeds/1052732854.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/defence/rssfeeds/46687796.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/company/rssfeeds/2143429.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/international/rssfeeds/858478126.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/elections/rssfeeds/65869819.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/et-explains/rssfeeds/64552206.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/sports/rssfeeds/26407562.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/science/rssfeeds/39872847.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/india-unlimited/rssfeeds/45228216.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/environment/rssfeeds/2647163.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        {"url" : "https://economictimes.indiatimes.com/news/latest-news/rssfeeds/20989204.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : None},

        #ET Industry
        {"url" : "https://economictimes.indiatimes.com/industry/auto/rssfeeds/13359412.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_auto"},

        {"url" : "https://economictimes.indiatimes.com/industry/banking/finance/rssfeeds/13358259.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_banking/finance"},

        {"url" : "https://economictimes.indiatimes.com/industry/cons-products/rssfeeds/13358759.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_cons-products"},

        {"url" : "https://economictimes.indiatimes.com/industry/energy/rssfeeds/13358350.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_energy"},

        {"url" : "https://economictimes.indiatimes.com/industry/indl-goods/svs/rssfeeds/13357688.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_indl-goods/svs"},

        {"url" : "https://economictimes.indiatimes.com/industry/healthcare/biotech/rssfeeds/13358050.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_healthcare/biotech"},

        {"url" : "https://economictimes.indiatimes.com/industry/services/rssfeeds/13354120.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_services"},

        {"url" : "https://economictimes.indiatimes.com/industry/media/entertainment/rssfeeds/13357212.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_media/entertainment"},

        {"url" : "https://economictimes.indiatimes.com/industry/transportation/rssfeeds/13353990.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_transportation"},

        {"url" : "https://economictimes.indiatimes.com/industry/tech/rssfeeds/56811438.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tech"},

        {"url" : "https://economictimes.indiatimes.com/industry/telecom/rssfeeds/13354103.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_telecom"},

        {"url" : "https://economictimes.indiatimes.com/industry/miscellaneous/rssfeeds/58456958.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_miscellaneous"},

        {"url" : "https://economictimes.indiatimes.com/industry/csr/rssfeeds/58571497.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_csr"},

        {"url" : "https://economictimes.indiatimes.com/industry/environment/rssfeeds/58571602.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_environment"},

        #ET Small Biz

        {"url" : "https://economictimes.indiatimes.com/small-biz/sme-sector/rssfeeds/11993058.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_smeSector"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/startups/rssfeeds/11993050.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_startups"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/policy-trends/rssfeeds/11993039.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_policy&trends"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/trade/rssfeeds/68806566.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_trade"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/entrepreneurship/rssfeeds/11993034.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_entrepreneurship"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/money/rssfeeds/47280660.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_money"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/security-tech/rssfeeds/47280820.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_it"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/legal/rssfeeds/47280656.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_legal"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/gst/rssfeeds/58475404.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_gst"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/productline/rssfeeds/68888444.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_productline"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/biz-listings/rssfeeds/54421370.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_bizListings"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/marketing-branding/rssfeeds/47280443.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_marketing-branding"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/hr-leadership/rssfeeds/47280670.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_hrLeadership"},

        {"url" : "https://economictimes.indiatimes.com/small-biz/resources/rssfeeds/59663072.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_resources"},

        #ET Wealth
        {"url" : "https://economictimes.indiatimes.com/wealth/tax/rssfeeds/47119912.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tax"},

        {"url" : "https://economictimes.indiatimes.com/wealth/save/rssfeeds/47119915.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_save"},

        {"url" : "https://economictimes.indiatimes.com/wealth/invest/rssfeeds/48997553.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_invest"},

        {"url" : "https://economictimes.indiatimes.com/wealth/insure/rssfeeds/47119917.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_insure"},

        {"url" : "https://economictimes.indiatimes.com/wealth/spend/rssfeeds/48997538.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_spend"},

        {"url" : "https://economictimes.indiatimes.com/wealth/borrow/rssfeeds/48997485.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_borrow"},

        {"url" : "https://economictimes.indiatimes.com/wealth/earn/rssfeeds/48997527.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_earn"},

        {"url" : "https://economictimes.indiatimes.com/wealth/plan/rssfeeds/49674351.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_plan"},

        {"url" : "https://economictimes.indiatimes.com/wealth/real-estate/rssfeeds/48997582.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_realEstate"},

        {"url" : "https://economictimes.indiatimes.com/wealth/personal-finance-news/rssfeeds/49674901.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_personalFinanceNews"},

        {"url" : "https://economictimes.indiatimes.com/wealth/podcast/rssfeeds/67899330.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_podcast"},

        {"url" : "https://economictimes.indiatimes.com/wealth/tomorrowmakers/rssfeeds/49995335.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tomorrowmakers"},
        
        {"url" : "https://economictimes.indiatimes.com/wealth/mutual-funds/rssfeeds/49995327.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mutualFunds"},

        {"url" : "https://economictimes.indiatimes.com/wealth/p2p/rssfeeds/65068593.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_p2p"},

        {"url" : "https://economictimes.indiatimes.com/wealth/data-center/rssfeeds/50960039.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_dataCenter"},

        {"url" : "https://economictimes.indiatimes.com/wealth/calculators/rssfeeds/5251408.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_calculators"},

        {"url" : "https://economictimes.indiatimes.com/wealth/buy-online/rssfeeds/49204774.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_buyOnline"},

        {"url" : "https://economictimes.indiatimes.com/wealth/interest-rates/rssfeeds/57409318.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_interestRates"},

        {"url" : "https://economictimes.indiatimes.com/wealth/participate-win/rssfeeds/50943022.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_participate&win"},

        {"url" : "https://economictimes.indiatimes.com/wealth/et-wealth/rssfeeds/50943048.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etWealth"},

        # ET MF
        {"url" : "https://economictimes.indiatimes.com/mf/mf-news/rssfeeds/1107225967.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mfNews"},

        {"url" : "https://economictimes.indiatimes.com/mf/analysis/rssfeeds/314856258.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_analysis"},

        {"url" : "https://economictimes.indiatimes.com/mf/mutual-fund-screener/rssfeeds/72180183.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mutualFundScreener"},

        {"url" : "https://economictimes.indiatimes.com/mf/elss/rssfeeds/64517243.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_elss"},

        {"url" : "https://economictimes.indiatimes.com/mf/learn/rssfeeds/54696002.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_learn"},

        {"url" : "https://economictimes.indiatimes.com/mf/etf/rssfeeds/8757630.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etf"},

        {"url" : "https://economictimes.indiatimes.com/mf/best-mutual-funds-to-buy/rssfeeds/63896891.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_bestMutualFundsToBuy"},

        {"url" : "https://economictimes.indiatimes.com/mf/nps/rssfeeds/58515020.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nps"},

        {"url" : "https://economictimes.indiatimes.com/mf/tools/rssfeeds/66461119.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tools"},

        {"url" : "https://economictimes.indiatimes.com/mf/mf-recategorization/rssfeeds/65037411.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mfRecategorization"},

        {"url" : "https://economictimes.indiatimes.com/mf/mutual-funds-stats/rssfeeds/58514674.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_mutualFundsStats"},

        # ET Tech
        {"url" : "https://economictimes.indiatimes.com/tech/hardware/rssfeeds/13357565.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_hardware"},

        {"url" : "https://economictimes.indiatimes.com/tech/software/rssfeeds/13357555.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_software"},

        {"url" : "https://economictimes.indiatimes.com/tech/internet/rssfeeds/13357549.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_internet"},

        {"url" : "https://economictimes.indiatimes.com/tech/ites/rssfeeds/40274504.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_ites"},

        {"url" : "https://economictimes.indiatimes.com/tech/tech-and-gadgets/rssfeeds/60529947.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_techAndGadgets"},

        {"url" : "https://economictimes.indiatimes.com/tech/startups/rssfeeds/78570540.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tech_startups"},

        # ET Opinion
        {"url" : "https://economictimes.indiatimes.com/opinion/et-view/rssfeeds/63829738.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etView"},

        {"url" : "https://economictimes.indiatimes.com/opinion/poke-me/rssfeeds/14425836.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_pokeMe"},

        {"url" : "https://economictimes.indiatimes.com/opinion/et-commentary/rssfeeds/3389985.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etCommentary"},

        {"url" : "https://economictimes.indiatimes.com/opinion/et-editorial/rssfeeds/3376910.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etEditorial"},

        {"url" : "https://economictimes.indiatimes.com/opinion/speaking-tree/rssfeeds/52109321.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_speakingTree"},

        {"url" : "https://economictimes.indiatimes.com/opinion/blogs/rssfeeds/46313130.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_blogs"},

        {"url" : "https://economictimes.indiatimes.com/opinion/et-citings/rssfeeds/18951109.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_etCitings"},

        {"url" : "https://economictimes.indiatimes.com/opinion/opinion-poll/rssfeeds/2424819.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_opinionPoll"},

        {"url" : "https://economictimes.indiatimes.com/opinion/interviews/rssfeeds/2184566.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_interviews"},

        {"url" : "https://economictimes.indiatimes.com/opinion/vedanta/rssfeeds/3384669.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_vedanta"},

        {"url" : "https://economictimes.indiatimes.com/opinion/qna/rssfeeds/5666504.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_qna"},

        # ET NRI
        {"url" : "https://economictimes.indiatimes.com/nri/nris-in-news/rssfeeds/7771300.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nrisInNews"},

        {"url" : "https://economictimes.indiatimes.com/nri/nri-real-estate/rssfeeds/7771290.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nriRealEstate"},

        {"url" : "https://economictimes.indiatimes.com/nri/nri-investments/rssfeeds/7771289.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nriInvestments"},

        {"url" : "https://economictimes.indiatimes.com/nri/nri-tax/rssfeeds/7771292.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nriTax"},

        {"url" : "https://economictimes.indiatimes.com/nri/forex-and-remittance/rssfeeds/7771285.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_forexAndRemittance"},

        {"url" : "https://economictimes.indiatimes.com/nri/visa-and-immigration/rssfeeds/7771304.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_visaAndImmigration"},

        {"url" : "https://economictimes.indiatimes.com/nri/working-abroad/rssfeeds/7771282.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_working-abroad"},

        {"url" : "https://economictimes.indiatimes.com/nri/returning-to-india/rssfeeds/7771276.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_returning-to-india"},

        {"url" : "https://economictimes.indiatimes.com/nri/nri-services/rssfeeds/2482844.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_nri-services"},

        # ET Magazines
        {"url" : "https://economictimes.indiatimes.com/magazines/panache/rssfeeds/32897557.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_panache"},

        {"url" : "https://economictimes.indiatimes.com/magazines/et-magazine/rssfeeds/7771003.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_et-magazine"},

        {"url" : "https://economictimes.indiatimes.com/magazines/travel/rssfeeds/640246854.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_travel"},

        # ET Video Feeds
        {"url" : "https://economictimes.indiatimes.com/et-now/stocks/rssfeedsvideo/4413765.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_stocks"},

        {"url" : "https://economictimes.indiatimes.com/et-now/experts/rssfeedsvideo/4413767.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_experts"},

        {"url" : "https://economictimes.indiatimes.com/et-now/corporate/rssfeedsvideo/4413769.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_corporate"},

        {"url" : "https://economictimes.indiatimes.com/et-now/markets/rssfeedsvideo/4413330.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_markets"},

        {"url" : "https://economictimes.indiatimes.com/et-now/auto/rssfeedsvideo/4715628.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_auto"},

        {"url" : "https://economictimes.indiatimes.com/et-now/tech/rssfeedsvideo/4413780.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_tech"},

        {"url" : "https://economictimes.indiatimes.com/et-now/policy/rssfeedsvideo/4413772.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_policy"},

        {"url" : "https://economictimes.indiatimes.com/et-now/finance/rssfeedsvideo/4413774.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_finance"},

        {"url" : "https://economictimes.indiatimes.com/et-now/commodities/rssfeedsvideo/4413784.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_commodities"},

        {"url" : "https://economictimes.indiatimes.com/et-now/daily/rssfeedsvideo/4424867.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_daily"},

        {"url" : "https://economictimes.indiatimes.com/et-now/et-promotions/rssfeedsvideo/6809269.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_et-promotions"},

        {"url" : "https://economictimes.indiatimes.com/et-now/brand-equity/rssfeedsvideo/4424935.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_brand-equity"},

        {"url" : "https://economictimes.indiatimes.com/et-now/entertainment/rssfeedsvideo/9443243.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_entertainment"},

        {"url" : "https://economictimes.indiatimes.com/et-now/results/rssfeedsvideo/4413789.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_results"},

        {"url" : "https://economictimes.indiatimes.com/et-now/budget/rssfeedsvideo/4729226.cms",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "economictimes_budget"},

        
        ##################################################
        ##################################################
        # Livemint
        {"url" : "https://www.livemint.com/rss/companies",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_companies"},

        {"url" : "https://www.livemint.com/rss/opinion",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_opinion"},

        {"url" : "https://www.livemint.com/rss/money",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_money"},

        {"url" : "https://www.livemint.com/rss/politics",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_politics"},

        {"url" : "https://www.livemint.com/rss/science",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_science"},

        {"url" : "https://www.livemint.com/rss/industry",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_industry"},

        {"url" : "https://www.livemint.com/rss/lounge",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_lounge"},

        {"url" : "https://www.livemint.com/rss/education",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_education"},

        {"url" : "https://www.livemint.com/rss/sports",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_sports"},

        {"url" : "https://www.livemint.com/rss/technology",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_technology"},

        {"url" : "https://www.livemint.com/rss/news",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_news"},

        {"url" : "https://www.livemint.com/rss/Mutual%20Funds",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_mutualFunds"},

        {"url" : "https://www.livemint.com/rss/markets",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_markets"},

        {"url" : "https://www.livemint.com/rss/AI",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_ai"},

        {"url" : "https://www.livemint.com/rss/insurance",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_insurance"},

        {"url" : "https://www.livemint.com/rss/budget",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_budget"},

        {"url" : "https://www.livemint.com/rss/elections",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_elections"},

        {"url" : "https://www.livemint.com/rss/videos",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description','author'],
        "match_list" : ['title','link','image','guid','pubDate','description','author'],
        "topic" : "livemint_videos"},

        ##################################################
        ##################################################
        # Bloomberg
        {"url" : "https://www.bloombergquint.com/stories.rss",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','{http://www.w3.org/2005/Atom}updated','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "bloomberg_stories"},

        ##################################################
        ##################################################
        # Business Today
        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=105",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_latest_news"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=100",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_bizWrap"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=111",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_markets"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=101",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_sectors"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=110",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_technology"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=109",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_money"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=103",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_management"},

        {"url" : "https://www.businesstoday.in/rss/rssstory.jsp?sid=102",
        "main_tag" : "item",
        "attr_list" : ['title','link','pubDate','description'],
        "match_list" : ['title','link','pubDate','description'],
        "topic" : "businesstoday_talkingHeads"},

        ##################################################
        ##################################################
        # Forbes India
        {"url" : "https://www.forbesindia.com/rssfeeds/rss_all.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_completeForbesIndia.comWebsite"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_magazine.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_completeMagazine"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mostemailed.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostEmailed"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_upfront.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_upFront"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mostcommented.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostCommented"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_features.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_features"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mostread.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostRead"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_life.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_life"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mosttdsemailed.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostEmailedInDailySabbatical"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_thedailysabbatical.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_dailySabbatical"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mosttdscommented.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostCommentedInDailySabbatical"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_multimedia.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_multimedia"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mosttdsread.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostReadInDailySabbatical"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_multimedia.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_blogs"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_mosttdsread.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostReadInDailySabbatical"},

        {"url" : "https://www.forbesindia.com/rssfeeds/rss_audio.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description'],
        "match_list" : ['title','link','guid','pubDate','description'],
        "topic" : "forbesindia_mostPopularAudio"},

        ##################################################
        ##################################################
        # Wion
        {"url" : "https://www.wionews.com/feeds/south-asia/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_southAsia"},

        {"url" : "https://www.wionews.com/feeds/world/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_world"},

        {"url" : "https://www.wionews.com/feeds/business-economy/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_business&Economy"},

        {"url" : "https://www.wionews.com/feeds/cricket/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_cricket"},

        {"url" : "https://www.wionews.com/feeds/sports/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_sports"},

        {"url" : "https://www.wionews.com/feeds/science-technology/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_science&Technology"},

        {"url" : "https://www.wionews.com/feeds/opinions-0/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_opinions"},

        {"url" : "https://www.wionews.com/feeds/india-news/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_indiaNews"},

        {"url" : "https://www.wionews.com/feeds/entertainment/rss.xml",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "wion_entertainment"},

        ## Blogs

        ##################################################
        ##################################################
        # CapitalMind
        {"url" : "https://www.capitalmind.in/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "capitalmind"},

        ##################################################
        ##################################################
        # Finshots
        {"url" : "https://finshots.in/rss/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','{http://search.yahoo.com/mrss/}content'],
        "match_list" : ['title','link','guid','pubDate','description','image'],
        "topic" : "finshots"},

        ##################################################
        ##################################################
        # Safal Niveshak
        {"url" : "https://www.safalniveshak.com/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "safalniveshak"},

        ##################################################
        ##################################################
        # India Spend
        {"url" : "https://www.indiaspend.com/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator','{http://purl.org/rss/1.0/modules/content/}encoded'],
        "match_list" : ['title','link','guid','pubDate','description','image','author', 'content'],
        "topic" : "indiaspend"},



        
        ##################################################
        ##################################################
        # Moneycontrol
        # {"url" : "http://www.moneycontrol.com/rss/MCtopnews.xml",
        # "main_tag" : "item",
        # "attr_list" : ['title','link','image','guid','pubDate','description'],
        # "match_list" : ['title','link','image','guid','pubDate','description'],
        # "topic" : "moneycontrol_mctopNews"},

        # {"url" : "http://www.moneycontrol.com/rss/buzzingstocks.xml",
        # "main_tag" : "item",
        # "attr_list" : ['title','link','image','guid','{http://www.w3.org/2005/Atom}updated','description'],
        # "match_list" : ['title','link','image','guid','pubDate','description'],
        # "topic" : "moneycontrol_buzzingStocks"}
        
        ]
    
    return rss_list


def get_blogs_rss_list():
    '''
        Returns a list of dictionary containing blogs information
        Start all the blog topics with keepup__blog__*. This is more like mangling.
        Since we do not have enough resources to scale up elasticsearch for a separate index, 
        we are using this approach to squeeze blogs as articles itself.
    '''
    rss_list = [

        ##################################################
        ##################################################
        # Aswath Damodaran Blog
        {"url" : "http://aswathdamodaran.blogspot.com/feeds/posts/default?alt=rss",
        "main_tag" : "item",
        "attr_list" : ['title','link','image','guid','pubDate','description'],
        "match_list" : ['title','link','image','guid','pubDate','description'],
        "topic" : "keepup__blog__aswath<space>damodaran" ,
        "tags"  : ["keepup__blog__aswath<space>damodaran"]
        },

        ##################################################
        ##################################################
        # CapitalMind
        {"url" : "https://www.capitalmind.in/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "keepup__blog__capitalmind" ,
        "tags"  : ["keepup__blog__capitalmind"]
        },

        ##################################################
        ##################################################
        # Finshots
        {"url" : "https://finshots.in/rss/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','{http://search.yahoo.com/mrss/}content'],
        "match_list" : ['title','link','guid','pubDate','description','image'],
        "topic" : "keepup__blog__finshots" ,
        "tags"  : ["keepup__blog__finshots"]
        },

        ##################################################
        ##################################################
        # Safal Niveshak
        {"url" : "https://www.safalniveshak.com/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "keepup__blog__safalniveshak" ,
        "tags"  : ["keepup__blog__safalniveshak"]
        },

        # The Generalist
        {"url" : "https://thegeneralist.substack.com/feed",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','enclosure','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "keepup__blog__the<space>generalist" ,
        "tags"  : ["keepup__blog__the<space>generalist"]
        },

        # Trends.vc
        # Removed because it needs subscription
        # {"url" : "https://rss.app/feeds/nKejGBjDbAsXvPhN.xml",
        # "main_tag" : "item",
        # "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        # "match_list" : ['title','link','guid','pubDate','description','image','author'],
        # "topic" : "keepup__blog__trends.vc" ,
        # "tags"  : ["keepup__blog__trends.vc"]
        # },

        # Logistics Insider
        {"url" : "https://www.logisticsinsider.in/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "keepup__blog__logistics<space>insider" ,
        "tags"  : ["keepup__blog__logistics<space>insider"]
        },

        # Better Interiors
        {"url" : "http://www.betterinteriors.in/feed/",
        "main_tag" : "item",
        "attr_list" : ['title','link','guid','pubDate','description','image','{http://purl.org/dc/elements/1.1/}creator'],
        "match_list" : ['title','link','guid','pubDate','description','image','author'],
        "topic" : "keepup__blog__better<space>interiors" ,
        "tags"  : ["keepup__blog__better<space>interiors"]
        },

        
    ]

    return rss_list