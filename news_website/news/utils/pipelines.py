from ..utils.database import DatabaseFurnisher
from ..utils.rss import RSSFeedProcessor
from .rss_feeds import get_blogs_rss_list

import logging

logger = logging.getLogger(__name__)

class UpdatePipeline :
    """
        This class acts as a base class and holds necessary
        classes to update to the database
    """
    def __init__(self, *args, **kwargs):
        self.rss_feed_processor = RSSFeedProcessor()
        self.database_furnisher = DatabaseFurnisher()


class BlogUpdatePipeline(UpdatePipeline):
    
    def run(self):
        blog_rss_list = get_blogs_rss_list()
        for blog_rss in blog_rss_list:
            news_articles = self.rss_feed_processor.get_rss_news(url=blog_rss['url'],
                                            main_tag=blog_rss['main_tag'],attr_list=blog_rss['attr_list'],
                                            match_list=blog_rss['match_list'])   
            self.database_furnisher.add_articles_to_db(news_articles=news_articles, topic=blog_rss["topic"],
                                                tags = blog_rss["tags"])