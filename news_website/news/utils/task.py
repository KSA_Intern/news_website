import pandas as pd
import os, re
import logging
import requests

from .database import DatabaseFurnisher
from .processing import Preprocessor
from .rss import RSSFeedProcessor
from ..models import Related_Topic, Topic, Company, Article, BlogTopic

logger = logging.getLogger(__file__)

rfp = RSSFeedProcessor()
preprocessor = Preprocessor

class TaskInterface(object):
    """
    docstring
    """
    def __init__(self):
        pass

    def run(self):
        pass

class TaskImplementor(object):
    """
    docstring
    """
    def __init__(self):
        self.task_queue = []

    def register(self, task:TaskInterface):
        """
        docstring
        """
        self.task_queue.append(task)
    
    def implement(self):
        """
        docstring
        """
        for task in self.task_queue:
            task.run()

class CompanyAdditionTask(TaskInterface):
    """
    docstring
    """
    def __init__(self):
        """
        docstring
        """
        self.company_index = 0

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        company_data = pd.read_csv(os.path.join(abs_path,"Company_Names - Company_Names.csv"))

        tot_len = len(company_data)
        for index in range(self.company_index, tot_len):
            print("index : ", index)
            if str(company_data.loc[index,"ISIN No"]).startswith("INF") or (pd.isnull(company_data.loc[index,"Security Name"])):
                continue
            processed_name = re.sub(r"(\$|-\$|LTD.*|Limited.*|limited.*|Ltd.*|LIMITED.*|ltd.*)*","",company_data.loc[index,"Security Name"])
            processed_name = re.sub(r'&#39;',"'",processed_name).strip().lower()
            rfp.fetch_from_google_rss(processed_name,add_article_to_topic=True)
            self.company_index = (index + 1) % tot_len


class CompanySectorAdditionTask(TaskInterface):
    """
    docstring
    """
    def __init__(self):
        """
        docstring
        """
        pass

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        company_data = pd.read_csv(os.path.join(abs_path,"Company_Names - Company_Names.csv"))
        topic_replacement_data = pd.read_csv(os.path.join(abs_path,"Topics and Its Feeds - Sheet1.csv"))

        for index in range(len(company_data)):
            if str(company_data.loc[index,"ISIN No"]).startswith("INF") or (pd.isnull(company_data.loc[index,"Security Name"])):
                continue
            processed_name = re.sub(r"(\$|-\$|LTD.*|Limited.*|limited.*|Ltd.*|LIMITED.*|ltd.*)*","",company_data.loc[index,"Security Name"])
            processed_company_name = re.sub(r'&#39;',"'",processed_name).strip().lower()
            try:
                company_instance, if_created = Company.objects.get_or_create(company_name=processed_company_name.lower())
            except Exception as e:
                logger.info(e)
                logger.info("Couldn't add the company "+ processed_company_name.lower())
            company_sector = company_data.loc[index, "Industry"]
            replacement = topic_replacement_data[topic_replacement_data["Sectors"]==company_sector]["Replacement"].values
            
            # If the sector info is not given skip the company and no topic of company is created
            if pd.isnull(company_sector):
                continue
            
            company_sector = str(company_sector).lower().strip()
            logger.info(processed_company_name+", "+company_sector)
            # If replacement for a sector is found use replacement sector
            if replacement.size==1 and not pd.isnull(replacement[0]):
                replacement_sector = str(replacement[0]).lower().strip()
                try:   
                    related_topic_instance, if_created = Related_Topic.objects.get_or_create(name=replacement_sector)
                    # main_topic_instance, if_created = Topic.objects.get_or_create(name=replacement_sector)
                    # company_sector_instance, if_created = Topic.objects.get_or_create(name=company_sector)
                except Exception as identifier:
                    logger.error(identifier)
                    logger.info("Unable to get or create topic of company's sector replacement - "+replacement_sector)
            # If replacement for a sector is not found use available sector
            else :
                try:   
                    related_topic_instance, if_created = Related_Topic.objects.get_or_create(name=company_sector)
                    # main_topic_instance, if_created = Topic.objects.get_or_create(name=company_sector)
                except Exception as identifier:
                    logger.error(identifier)
                    logger.info("Unable to get or create topic of company's sector - "+company_sector)
            
            # Get or create company topic
            try:
                company_topic, if_created = Topic.objects.get_or_create(name=processed_company_name)
            except Exception as identifier:
                logger.error(identifier)
                logger.info("Unable to get or create topic of company - "+processed_company_name)
            
            if related_topic_instance and company_topic:
                related_topic_instance.topics.add(company_topic)
                # logger.info(f"Before for {main_topic_instance.name} : {len(main_topic_instance.related_articles.all())}")
                # if main_topic_instance:
                #     main_topic_instance.related_articles.add(*company_topic.related_articles.all())
                #     main_topic_instance.save()
                # if company_sector_instance:
                #     company_sector_instance.related_articles.add(*company_topic.related_articles.all())
                #     company_sector_instance.save()
                # logger.info(f"After for {main_topic_instance.name} : {len(main_topic_instance.related_articles.all())}")
                related_topic_instance.save()

class RelatedTopicMergeTask(TaskInterface):
    """
    docstring
    """
    def __init__(self):
        """
        docstring
        """
        pass

    def run(self):
        all_topics = Related_Topic.objects.all()
        for each_topic in all_topics:
            try:
                topic, if_created = Topic.objects.get_or_create(name=each_topic.name.lower().strip())
                if if_created:
                    logger.info("The topic unable to fetch "+each_topic.name)
                # print(f"Before for {topic.name} : {len(topic.related_articles.all())}")
                for related_topic in each_topic.topics.all():
                    try:
                        topic.related_articles.add(*related_topic.related_articles.all())
                        topic.save()
                    except Exception as e:
                        logger.error(e)
                # print(f"After for {topic.name} : {len(topic.related_articles.all())}")
            except Exception as e:
                logger.error(e)


class SectorRSSAdditionTask(TaskInterface):
    """
    Add RSS feeds as related topic for sectors
    """
    def __init__(self):
        """
        docstring
        """
        pass

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        topic_replacement_data = pd.read_csv(os.path.join(abs_path,"Topics and Its Feeds - Sheet1.csv"))

        for index in range(len(topic_replacement_data)):
            sector = topic_replacement_data.loc[index,"Sectors"]
            replacement_sector = topic_replacement_data.loc[index,"Replacement"]
            if (pd.isnull(replacement_sector)) and (pd.isnull(sector)): continue

            try:
                if not pd.isnull(replacement_sector):
                    replacement_sector_instance, if_created = Related_Topic.objects.get_or_create(name=replacement_sector.lower().strip())
                    if if_created:
                        logger.info("Newly created Related Topic - "+replacement_sector)
                if not pd.isnull(sector):
                    sector_instance, if_created = Related_Topic.objects.get_or_create(name=sector.lower().strip())
                    if if_created:
                        logger.info("Newly created Related Topic - "+sector)
                for related_topic in topic_replacement_data.loc[index][2:]:
                    if not pd.isnull(related_topic):
                        topic, if_created = Topic.objects.get_or_create(name=str(related_topic).lower().strip())
                        if replacement_sector:
                            replacement_sector_instance.topics.add(topic)
                            replacement_sector_instance.save()
                        if sector:
                            sector_instance.topics.add(topic)
                            sector_instance.save()
            except Exception as e:
                logger.error(e)


class ArticleCompanyLinkingTask(TaskInterface):
    """
    Maually checks all articles and adds them to the company topics if
    company name exists in the title.

    Note: This task assumes that the company names are added into database
          under Company model.
    """
    def __init__(self):
        """
        docstring
        """
        pass

    def run(self):
        companies = Company.objects.all()
        # articles = Article.objects.all()

        for company in companies:
            com_name = company.company_name
            # Brute force
            # for article in articles:
            #     art_title = article.title
            #     if com_name.lower() in art_title.lower():
            #         try:
            #             # company_topic = Topic.objects.get_or_create(name=com_name)
            #             # company_topic.related_articles.add(article)
            #             logger.info(art_title + " -- " + com_name)
            #         except Exception as e:
            #             logger.error(e)

            company_articles = Article.objects.filter(title__icontains=com_name)
            try:
                company_topic, if_created = Topic.objects.get_or_create(name=com_name)
                company_topic.related_articles.add(*company_articles)
                # logger.info(art_title + " -- " + com_name)
            except Exception as e:
                logger.error(e)


class RevertBackTheBlunder(TaskInterface):
    """
    The production database is ruined. 
    Should have taken the backup of database before pushing into production.
    This function tries to revert back the change.
    """
    def __init__(self):
        """
        docstring
        """
        pass
    
    def string_found(self, string1, string2):
        if re.search(r"\b" + re.escape(string1) + r"\b", string2):
            return True
        return False

    def run(self):
        companies = Company.objects.all()
        related_topics = Related_Topic.objects.all()

        for reltopic in related_topics:
            thetopic = Topic.objects.get(name=reltopic.name)
            thetopic.related_articles.remove(*thetopic.related_articles.all())
            thetopic.save()

        for company in companies:
            com_name = company.company_name.lower()

            topic_com, if_created = Topic.objects.get_or_create(name=com_name.lower())

            company_articles = topic_com.related_articles.all()
            for article in company_articles:
                art = article.title.lower()
                if self.string_found(com_name , art):
                    pass
                else:
                    topic_com.related_articles.remove(article)
                topic_com.save()
        
        RelatedTopicMergeTask().run()
    
    
class DownloadGoogleSheetTask(TaskInterface):
    """
    This task downloads the google sheet from the given
    download link and stores it in a csv file.
    """
    def __init__(self):
        """
        docstring
        """
        pass
    
    def download_given_url(self,url, destination):
        """
            This function is written to download publicly shared google sheets
            with the given download url and destination. `destination` is the 
            place where the file will be downloaded to
        """
        def get_confirm_token(response):
            for key, value in response.cookies.items():
                if key.startswith('download_warning'):
                    return value

            return None

        def save_response_content(response, destination):
            CHUNK_SIZE = 32768

            with open(destination, "wb") as f:
                for chunk in response.iter_content(CHUNK_SIZE):
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)

        URL = url

        session = requests.Session()

        response = session.get(URL, params = {  }, stream = True)
        token = get_confirm_token(response)
        
        if token:
            params = {  'confirm' : token }
            response = session.get(URL, params = params, stream = True)
        
        if response.status_code!=200 : return

        save_response_content(response, destination)

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        self.download_given_url("https://docs.google.com/spreadsheets/u/2/d/1yWAoqeQJGtAv2ZJ3CEoy1Z118aymYLHbCj9cirxDTdk/export?format=csv&id=1yWAoqeQJGtAv2ZJ3CEoy1Z118aymYLHbCj9cirxDTdk&gid=0",os.path.join(abs_path,"Concalls Today.csv"))


class DownloadGoogleSheetTrendingTopicsTask(TaskInterface):
    """
    This task downloads the google sheet from the given
    download link and stores it in a csv file.
    """
    def __init__(self):
        """
        docstring
        """
        pass
    
    def download_given_url(self,url, destination):
        """
            This function is written to download publicly shared google sheets
            with the given download url and destination. `destination` is the 
            place where the file will be downloaded to
        """
        def get_confirm_token(response):
            for key, value in response.cookies.items():
                if key.startswith('download_warning'):
                    return value

            return None

        def save_response_content(response, destination):
            CHUNK_SIZE = 32768

            with open(destination, "wb") as f:
                for chunk in response.iter_content(CHUNK_SIZE):
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)

        URL = url

        session = requests.Session()

        response = session.get(URL, params = {  }, stream = True)
        token = get_confirm_token(response)
        
        if token:
            params = {  'confirm' : token }
            response = session.get(URL, params = params, stream = True)
        
        if response.status_code!=200 : return

        save_response_content(response, destination)

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        self.download_given_url("https://docs.google.com/spreadsheets/u/2/d/1lvSN41_J2VrgmvCiQWBp794-s3UHOXawL3oSxSeeBH8/export?format=csv&id=1lvSN41_J2VrgmvCiQWBp794-s3UHOXawL3oSxSeeBH8&gid=0",os.path.join(abs_path,"Trending Topics.csv"))


class BlogTopicsAdditionTask(TaskInterface):
    """
    Add RSS feeds as related topic for sectors
    """
    def __init__(self):
        """
        docstring
        """
        pass

    def run(self):
        abs_path = os.path.dirname(os.path.abspath(__file__))
        blog_info_data = pd.read_csv(os.path.join(abs_path,"RSS Feeds - Blogs.csv"))

        i = 0
        for index in range(len(blog_info_data)):
            i = i + 1
            display_name = blog_info_data.loc[index,"Name"]
            if (pd.isnull(display_name)): continue

            try:
                processed_name = "keepup__blog__" + re.sub(r' ',"<space>",display_name).lower().strip()
                blog_topic_instance, if_created = BlogTopic.objects.get_or_create(name=processed_name)
                blog_topic_instance.display_name = display_name.strip()
                if if_created:
                    logger.info("Newly created Blog Topic - "+display_name)
                
            except Exception as e:
                logger.error(e)
                try :
                    topic_instance,if_created = Topic.objects.get_or_create(name=processed_name)
                    articles = topic_instance.related_articles.all()
                    topic_instance.delete()
                    blog_topic, if_created = BlogTopic.objects.get_or_create(name=processed_name,display_name=display_name.strip())
                    blog_topic.related_articles.add(*articles)
                    
                except Exception as e:
                    logger.error(e)
                