import pandas as pd

class FilterInterface(object):
    """
    docstring
    """
    def __init__(self):
        pass

    def filter(self, news_articles):
        pass

class Filters(object):
    """
    Filters for news articles
    """
    def __init__(self):
        self.filters_enabled_map = {}
        self.filters_order_list = []
    
    def register(self, filter:FilterInterface):
        """
          Register your filter
        """
        self.filters_order_list.append(filter)
    
    def filterall(self, news_articles):
        all_news = news_articles
        for filter in self.filters_order_list:
            all_news = filter.filter(all_news)
        
        return all_news

class GoogleRSSFeedFilter(FilterInterface):
    """
     Allows only those articles mentioned in the csv file
    """
    pass