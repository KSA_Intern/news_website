from ..models import Topic, Article, Source, UserTopics, SecondaryArticle
import requests 
import xml.etree.ElementTree as ET
import logging

from .database import DatabaseFurnisher
from .processing import Preprocessor

logger = logging.getLogger(__name__)

class RSSNewsSources(object):
    """
    docstring
    """
    def __init__(self):
        pass

    def get_source(self, url):
        """
        docstring
        """
        # Add only those sources which you are following.
        # Because google news rss uses this function to
        # find out if the news article is already a part
        # of our existing following source
        if "economictimes" in url:
            return "Economic Times"
        elif "bloombergquint" in url:
            return "Bloomberg Quint"
        elif "livemint" in url:
            return "Livemint"
        # elif "financialexpress" in url:
        #     return "Financial Express"
        elif "businesstoday" in url:
            return "Business Today"
        elif "forbesindia" in url:
            return "Forbes India"
        elif "wionews" in url:
            return "Wion"
        elif "capitalmind" in url:
            return "Capitalmind"
        elif "finshots" in url:
            return "Finshots"
        elif "safalniveshak" in url:
            return "Safal Niveshak"
        elif "indiaspend" in url:
            return "India Spend"
        else :
            return ""


class RSSFeedProcessor(object):
    """
    docstring
    """
    def __init__(self):
        self.sources = RSSNewsSources()
    
    def get_rss_news(self, url='https://economictimes.indiatimes.com/rssfeedsdefault.cms', 
                 main_tag='item',attr_list=['title','link','image','guid','pubDate','description'],
                 match_list = ['title','link','image','guid','pubDate','description']):
        '''
        Fetches rss feed from given url. 
        How to use ? 
        Pass the url of the rss feed as `url` attribute. Pass the name of the XML tag which contains
        information about the news article as `main_tag` attribute. Pass the list of the attributes
        or the tags that are children of main_tag that is to be fetched as `attr_list` attribute.
        `match_list` conains sequence of attributes - ['title','link','image','guid','pubDate','description']-
        in order that matches the attr_list. i.e if the above list is match_list, then attr_list[0]
        contains the corresponding attribute that fetches info related to match_list[0] - in this case it's 
        title. So attr_list[0] has attribute which fetches title

        The function returns list of dictionary where each entry is an article
        '''
        try:
            response = requests.get(url, headers= {"user-agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36"})
            if response.status_code == 404:
                return []
        except Exception as e:
            logger.info("Unable to fetch the url"+url)
            logger.error(e)
            return []
        try:
            if response.text:
                root = ET.fromstring(response.text)
            else:
                return []
        except Exception as e:
            logger.info("Unable to parse the URL "+url)
            logger.error(e)
            return []
        articles = []
        try:
            items = root.iter(str(main_tag))
        except Exception as e:
            logger.info(e)
            items = []
        for item in items:
            article = {}
            for index, attr in enumerate(attr_list):
                try:
                    result = item.find(attr)
                    if result.text:
                        article[match_list[index]] = result.text
                    elif result.attrib['url'] :
                        article[match_list[index]] = result.attrib['url']
                    else :
                        article[match_list[index]] = ''
                except :
                    article[match_list[index]] = ''
            articles.append(article)
        return articles
    

    # TODO This function is written when there was no idea of pipeline. Hence this function uses database
    #      utility. This might create circular dependancy in future. Hence, need to stop this function
    #      at just the processing of articles and return articles
    def fetch_from_google_rss(self, query_str,add_article_to_topic=False):
        dbf = DatabaseFurnisher()
        preprocessor = Preprocessor()
        query = str(query_str).lower().strip()
        query_rss = [
            {"url" : "https://news.google.com/rss/search?q=<<query>>&hl=en-IN&gl=IN&ceid=IN:en",
            "main_tag" : "item",
            "attr_list" : ['title','link','guid','pubDate','description'],
            "match_list" : ['title','link','guid','pubDate','description'],
            "topic" : "google_news"},
        ]

        for rss_item in query_rss:
            url = str(rss_item['url']).replace("<<query>>",str(query).strip())
            logger.info("Getting news from "+url)
            news_articles = self.get_rss_news(url, rss_item['main_tag'], rss_item['attr_list'], rss_item['match_list'])
            if news_articles:
                for news_article in news_articles:
                    sour = self.sources.get_source(news_article["link"])
                    if sour:
                        if rss_item["topic"] == "google_news":
                            dbf.add_articles_to_secondary_db([news_article],SecondaryArticle,topic = str(query), google_rss=True)
                        else :
                            dbf.add_articles_to_secondary_db([news_article],SecondaryArticle,topic = str(query))
                        
                        logger.debug("URL has "+sour+". Adding to secondary db")
                    else:
                        if preprocessor.found_in_allowed_urls(news_article["link"]):
                            if rss_item["topic"] == "google_news":
                                dbf.add_articles_to_db([news_article], topic=str(query), google_rss=True,add_article_to_topic=add_article_to_topic)
                            else:
                                dbf.add_articles_to_db([news_article], topic=str(query))
                            logger.debug("Added to Primary DB - URL was "+news_article["link"])