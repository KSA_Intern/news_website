from django.contrib import admin
from .models import (Source, Article, Topic, Category_Relation, UserTopics, Company, TimeLogger,
            Category, DefaultTopic, SecondaryArticle, Topic_Cluster, Related_Topic, BlogTopic, UserBlogTopics)

class TopicAdmin(admin.ModelAdmin):
    model = Topic
    fields = ['name']
    list_display = ['name', 'articles_related']

    def articles_related(self, obj):
        return [article for article in obj.related_articles.all()]

# Register your models here.
admin.site.register(Source)
admin.site.register(Article)
# admin.site.register(Topic, TopicAdmin)
admin.site.register(Topic)
admin.site.register(BlogTopic)
admin.site.register(UserTopics)
admin.site.register(UserBlogTopics)
admin.site.register(Category_Relation)
admin.site.register(Category)
admin.site.register(Company)
admin.site.register(DefaultTopic)
admin.site.register(SecondaryArticle)
admin.site.register(Topic_Cluster)
admin.site.register(Related_Topic)
admin.site.register(TimeLogger)