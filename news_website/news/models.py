from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Source(models.Model):
    id = models.BigAutoField(primary_key=True)
    api_id = models.CharField(max_length=512, null=True, blank=True)
    name = models.CharField(max_length=512, unique=True)
    description = models.TextField( null=True, blank=True)
    url = models.URLField(max_length=1024, null=True, blank=True)
    category = models.CharField(max_length=512, null=True, blank=True)
    language = models.CharField(max_length=32, null=True, blank=True)
    country = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class Company(models.Model):
    company_name = models.CharField(max_length=100, unique=True)
    company_scrip_id = models.IntegerField(null=True, blank=True)
    followers = models.ManyToManyField(
        get_user_model(), related_name='company_followers', blank=True)

    def __str__(self):
        return self.company_name


class Article(models.Model):
    id = models.BigAutoField(primary_key=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE,null=True, blank=True)
    author = models.CharField(max_length=512, null=True, blank=True)
    title = models.CharField(max_length=512 ,unique=True)
    url = models.URLField(max_length=1024)
    urlToImage = models.URLField(max_length=1024, null=True)
    publishedAt = models.CharField(max_length=32)
    # publishedAtFormatted = models.DateTimeField()
    content = models.CharField(max_length=1024,null=True, blank=True)
    description = models.TextField(null=True,blank=True)
    tags = models.TextField(null=True,blank=True)

    class Meta:
        ordering = ['-publishedAt']
    
    # def save(self, *args, **kwargs):
    #     import datetime
    #     dateTimeObject = datetime.datetime.strptime(str(self.publishedAt)[:19], "%Y-%m-%dT%H:%M:%S")
    #     self.publishedAtFormatted = dateTimeObject  
    #     super(Article, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.title

class SecondaryArticle(models.Model):
    id = models.BigAutoField(primary_key=True)
    source = models.CharField(max_length=512, null=True, blank=True)
    author = models.CharField(max_length=512, null=True, blank=True)
    title = models.CharField(max_length=512 ,unique=True)
    url = models.URLField(max_length=1024)
    urlToImage = models.URLField(max_length=1024, null=True)
    publishedAt = models.CharField(max_length=32)
    # publishedAtFormatted = models.DateTimeField()
    content = models.CharField(max_length=1024,null=True, blank=True)
    description = models.TextField(null=True,blank=True)
    keywords = models.TextField(null=True,blank=True)
    topic = models.TextField(null=True,blank=True)
    tags = models.TextField(null=True,blank=True)

    class Meta:
        ordering = ['-publishedAt']
    
    # def save(self, *args, **kwargs):
    #     import datetime
    #     dateTimeObject = datetime.datetime.strptime(str(self.publishedAt)[:19], "%Y-%m-%dT%H:%M:%S")
    #     self.publishedAtFormatted = dateTimeObject  
    #     super(Article, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.title
    
class Topic(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255, unique=True, db_index=True)
    related_articles = models.ManyToManyField(
                                Article, blank=True,
                                through='Category_Relation')
    
    def __str__(self):
        return self.name


class BlogTopic(Topic):
    blog_id = models.BigAutoField(primary_key=True)
    display_name = models.CharField(max_length=255, null=True, blank=True)



class Category_Relation(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['topic', 'article']
        verbose_name = _("Category_Relation")
        verbose_name_plural = _("Category_Relations")
        
    
    def __str__(self):
        return self.topic.name + " : " + self.article.title
    
class UserTopics(models.Model):
    topic = models.ForeignKey(Topic,  on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        unique_together = ['topic', 'user']
        verbose_name_plural = _("UserTopics")
            
    def __str__(self):
        return self.topic.name + ' : ' + self.user.email


class UserBlogTopics(models.Model):
    blog_topic = models.ForeignKey(BlogTopic,  on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        unique_together = ['blog_topic', 'user']
        verbose_name_plural = _("UserBlogTopics")
            
    def __str__(self):
        return self.blog_topic.display_name + ' : ' + self.user.email


class Topic_Cluster(models.Model):
    topics =  models.ManyToManyField(Topic, blank=True)
    name = models.CharField(max_length=1024, unique=True)
    description = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = _("Topic_Clusters")

    def __str__(self):
        return self.name

class Related_Topic(models.Model):
    name = models.CharField(max_length=1024, unique=True, db_index=True)
    topics =  models.ManyToManyField(Topic, blank=True) 

    class Meta:
        verbose_name = _("Related_Topic")
        verbose_name_plural = _("Related_Topics")

    def __str__(self):
        return self.name

class TimeLogger(models.Model):
    name = models.CharField(max_length=1024, unique=True, db_index=True)
    time_lapsed = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name = _("TimeLogger")
        verbose_name_plural = _("TimeLoggers")

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse("TimeLoggers_detail", kwargs={"pk": self.pk})


class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    class Meta:
        unique_together = ['name']
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.lower().strip()
        return super(Category, self).save(*args, **kwargs)

    # def get_absolute_url(self):
    #     return reverse("Categoriesdetail", kwargs={"pk": self.pk})


class DefaultTopic(models.Model):
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['topic','category']
        verbose_name = _("DefaultTopic")
        verbose_name_plural = _("DefaultTopics")

    def __str__(self):
        return self.topic.name + " : " + self.category.name

    