# Generated by Django 3.0.10 on 2020-11-04 06:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0011_auto_20201023_1147'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopicCluster',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=1024, unique=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('topics', models.ManyToManyField(blank=True, to='news.Topic')),
            ],
            options={
                'verbose_name_plural': 'TopicClusters',
            },
        ),
    ]
