from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django import forms

User = get_user_model()


class UserChangeForm(admin_forms.UserChangeForm):
    class Meta(admin_forms.UserChangeForm.Meta):
        model = User


class UserCreationForm(admin_forms.UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].help_text = None
        self.fields['password1'].widget.attrs.update({'placeholder':_('Password')})
    
    error_message = admin_forms.UserCreationForm.error_messages.update(
                {"duplicate_email": _("This email has already been taken.")}
            )
    password2 = None

    class Meta(admin_forms.UserCreationForm.Meta):
        model = User
        fields = ('name','email')
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name'}),
            'email': forms.EmailInput(
                attrs={'placeholder': 'E-mail'}),
            'password1': forms.PasswordInput(attrs={'placeholder': 'Password'})
        }

    def clean_email(self):
        email = self.cleaned_data["email"]

        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email

        raise ValidationError(self.error_messages["duplicate_email"])
