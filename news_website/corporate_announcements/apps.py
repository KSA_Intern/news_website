from django.apps import AppConfig


class CorporateAnnouncementsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'corporate_announcements'
