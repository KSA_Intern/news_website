from celery.decorators import periodic_task
from datetime import timedelta
import time

from ..news.models import TimeLogger
from ..news.tasks import TaskImplementor
from .utils.task import AnnouncementCompanyAdditionTask, OtherCompaniesAdditionTask

import logging

logger = logging.getLogger(__name__)


# @periodic_task(run_every=timedelta(minutes=10), soft_time_limit=600)
def run_once_only_tasks():
    start = time.time()
    try:
        has_run_bool, if_run = TimeLogger.objects.get_or_create(name="add_corporate_announcement_companies_to_db-if run")
        # logger.info("*"*1000)
        logger.info(has_run_bool.time_lapsed)
        logger.info(if_run)
        if if_run or not has_run_bool.time_lapsed:
            # logger.info("*"*1000)
            has_run_bool.time_lapsed = 1
            has_run_bool.save()
            start = time.time()
            implementor = TaskImplementor()
            implementor.register(AnnouncementCompanyAdditionTask())
            implementor.implement()
            try:
                timelog, if_created = TimeLogger.objects.get_or_create(name="add_corporate_announcement_companies_to_db-finish time")
                if not timelog.time_lapsed or timelog.time_lapsed < time.time() - start:
                    timelog.time_lapsed = time.time() - start
                timelog.save()
            except Exception as e:
                logger.error(e)
    except Exception as e:
        logger.error(e)

    start = time.time()
    try:
        has_run_bool, if_run = TimeLogger.objects.get_or_create(name="add_more_corporate_announcement_companies_to_db-if run")
        # logger.info("*"*1000)
        logger.info(has_run_bool.time_lapsed)
        logger.info(if_run)
        if if_run or not has_run_bool.time_lapsed:
            # logger.info("*"*1000)
            has_run_bool.time_lapsed = 1
            has_run_bool.save()
            start = time.time()
            implementor = TaskImplementor()
            implementor.register(OtherCompaniesAdditionTask())
            implementor.implement()
            try:
                timelog, if_created = TimeLogger.objects.get_or_create(name="add_more_corporate_announcement_companies_to_db-finish time")
                if not timelog.time_lapsed or timelog.time_lapsed < time.time() - start:
                    timelog.time_lapsed = time.time() - start
                timelog.save()
            except Exception as e:
                logger.error(e)
    except Exception as e:
        logger.error(e)