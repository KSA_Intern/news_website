"""news_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include, re_path
from . import views

app_name = "corporate_announcements"

urlpatterns = [
    re_path(
        route=r'^announcements/all/$',
        view=views.corporate_announcements,
        name='corporate_announcements',
    ),
    re_path(
        route=r'^announcements/home/$',
        view=views.home_announcements,
        name='home_announcements',
    ),
    re_path(
        route=r'^announcements/discover/$',
        view=views.discover_announcements,
        name='discover_announcements',
    ),
    re_path(
        route=r'^api/fetch/announcements/$',
        view=views.fetch_announcements,
        name='fetch_announcements_from_server',
    ),

    re_path(
        route=r'^api/company/following/$',
        view=views.following_companies,
        name='following_companies',
    ),
    re_path(
        route=r'^api/fetchForOldUser/$',
        view=views.fetch_announcements_for_old_user,
        name='fetch_announcements',
    ),
    re_path(
        route=r'^api/autocomplete/$',
        view=views.autocomplete,
        name='autocomplete',
    ),
    re_path(
        route=r'^api/follow/$',
        view=views.follow_company,
        name='follow_company',
    ),
    re_path(
        route=r'^api/unfollow/$',
        view=views.unfollow_company,
        name='unfollow_company',
    ),
    # re_path(
    #     route=r'^api/fetch/announcements/$',
    #     view=views.fetch_announcements_from_server,
    #     name='fetch_announcements_from_server',
    # ),
]