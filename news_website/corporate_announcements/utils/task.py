import os, json

from ...news.utils.task import TaskInterface
from ..models import Corpann_Company

import logging
abs_path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger(__name__)

class AnnouncementCompanyAdditionTask(TaskInterface):
    """
    docstring
    """
    def __init__(self):
        """
        docstring
        """
        self.company_index = 0

    def run(self):
        
        with open(os.path.join(abs_path,"company_data.json"),"r") as f:
            company_list = json.loads(f.read())

        for itm in company_list:
            name = itm["fields"]["company_name"]
            scrip_id = itm["fields"]["company_scrip_id"]
            try:
                new_company, is_created = Corpann_Company.objects.get_or_create(company_name = name,company_scrip_id = scrip_id)
                if is_created:
                    logger.info(name+ " - Company created")
            except:
                logger.error("Couldn't create corporate announcement company "+ name)

    
class OtherCompaniesAdditionTask(TaskInterface):
    """
        docstring
    """

    def __init__(self):
        """
        docstring
        """
        self.company_index = 0

    def run(self):
        
        with open(os.path.join(abs_path,"input_all_companies.json"),"r") as f:
            comp_data = json.loads(f.read())

        for itm in comp_data["data"]:
            name = itm["company_name"]
            scrip_id = itm["company_scrip_id"]
            try:
                new_company, is_created = Corpann_Company.objects.get_or_create(company_name = name,company_scrip_id = scrip_id)
                if is_created:
                    logger.info(name+ " - Company created")
            except:
                logger.error("Couldn't create  company "+ name)