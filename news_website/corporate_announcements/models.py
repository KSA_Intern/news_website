from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Corpann_Company(models.Model):
    company_name = models.CharField(max_length=100, unique=True)
    company_scrip_id = models.IntegerField(null=True, blank=True, unique=True)

    def __str__(self):
        return self.company_name


class User_Companies(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    company = models.ForeignKey(Corpann_Company, on_delete=models.CASCADE)

    class Meta:
        unique_together = ['user', 'company']
        verbose_name_plural = _('Users_Companies')

    def __str__(self):
        return self.user.email + ' : ' + self.company.company_name