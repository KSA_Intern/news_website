from django.contrib.auth import login
from django.shortcuts import render
from django.http import JsonResponse, response
from django.contrib.auth.decorators import login_required
from .models import Corpann_Company, User_Companies

import requests
import json

import logging

logger = logging.getLogger(__name__)

ANNOUNCEMENTS_RANGE_START = 0
ANNOUNCEMENTS_RANGE_END = 20

# Create your views here.

def autocomplete(request):
    substr = request.GET.get('search')
    data = []
    companies = Corpann_Company.objects.filter(company_name__iregex = rf'(.*){substr}(.*)').values("company_name","company_scrip_id")
    data.extend(companies)
    # data = list(set(data))
    ret = {"data" : []}
    for v in data:
        ret['data'].append({"company_name":v['company_name'].upper(), "scrip_id":v['company_scrip_id']})
    return JsonResponse(ret)


@login_required
def home_announcements(request):
    return render(request,'corporate_announcements/announcements_home.html',{})

@login_required
def corporate_announcements(request):
    return render(request,'corporate_announcements/announcements.html',{})


@login_required
def discover_announcements(request):
    return render(request,'corporate_announcements/announcements_discover.html',{})

@login_required
def fetch_announcements_for_old_user(request):
    URL = 'http://165.22.209.84/refresh_feed'
    params = {'email': request.user.email}
    api_response = requests.get(URL, params=params)
    if api_response.status_code!=200:
        return JsonResponse({'data':[]})
    
    json_data = {'data':api_response.json()['data']}

    return JsonResponse(json_data)


@login_required
def following_companies(request):
    following_companies = list(set(User_Companies.objects.filter(user=request.user).values_list('company__company_name','company__company_scrip_id')))
    ret = {"data" : []}
    for comp_data in following_companies:
        ret["data"].append({"company_name":comp_data[0],"scrip_id":comp_data[1]})

    return JsonResponse(ret)


@login_required
def follow_company(request):
    res = {"data":{
        "company_status" : "UnConfirmed",
        "scrip_id" : -1
    }}
    if request.method == "GET":
        company_scrip_id = request.GET.get("company")
        try:
            company = Corpann_Company.objects.get(company_scrip_id=int(company_scrip_id))
            try:
                user_companies_obj, is_created = User_Companies.objects.get_or_create(user=request.user, company=company)
                res = {"data" : {
                    "company_status" : "Confirmed",
                    "scrip_id" : company.company_scrip_id
                }}
                return JsonResponse(res)
            except:
                return JsonResponse(res)
        except:
            return JsonResponse(res)
    return JsonResponse(res)

@login_required
def unfollow_company(request):
    res = {"data":{
        "company_status" : "UnConfirmed",
        "scrip_id" : -1
    }}
    if request.method == "GET":
        company_scrip_id = request.GET.get("company")
        try:
            company = Corpann_Company.objects.get(company_scrip_id=int(company_scrip_id))
            try:
                user_companies_obj = User_Companies.objects.get(user=request.user, company=company)
                user_companies_obj.delete()
                res = {"data" : {
                    "company_status" : "Confirmed",
                    "scrip_id" : company.company_scrip_id
                }}
                return JsonResponse(res)
            except:
                return JsonResponse(res)
        except:
            return JsonResponse(res)
    return JsonResponse(res)

@login_required
def fetch_announcements(request):
    companies_followed_by_user = User_Companies.objects.filter(user=request.user).values_list('company__company_scrip_id',flat=True)
    range_begin = ANNOUNCEMENTS_RANGE_START
    range_end = ANNOUNCEMENTS_RANGE_END

    URL = 'http://165.22.209.84/corpann/get_announcements/'
    params = {'companies': companies_followed_by_user,'rangeBegin':ANNOUNCEMENTS_RANGE_START,
    'rangeEnd': ANNOUNCEMENTS_RANGE_END}
    response = requests.get(URL, params=params)
    json_data = {'data':response.json()['data']}
    return JsonResponse(json_data)


