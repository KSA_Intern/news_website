$("#dv-autocomplete").autocomplete({
    source: "/news/autocomplete/",
    minLength: 1,
    open: function(){
        setTimeout(function () {
            $('.ui-autocomplete').css('z-index', 999);
        }, 0);
    }
  });

function randomColours(){
    var allowedColours = ["#a74393","#525cea","#d74769","#7244a2","#d65142"];
    const random = Math.floor(Math.random() * allowedColours.length);
    return allowedColours[random]
  }

  $(document).ready(function(){
      $(".random-colour").each(function(){
          var colour = randomColours();
          $(this).css("background-color",colour);
      })
  });

  $(function(){
    $(".btn-follow").on('click',function () {
       $(this).text(function(i, text){
           return "Following";
       })
       $(this).css({'font-size':'12px'});
    });
 });
