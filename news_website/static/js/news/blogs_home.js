
STATIC_URL = "/static/"
const tabs = document.querySelectorAll("[data-tab-target]");
const tabContents = document.querySelectorAll("[data-tab-content]");
const hostname = window.location.host;
let page = 1;
let currentscrollHeight= 0;
let lockScroll= false;

tabs.forEach((tab) => {
  tab.addEventListener("click", () => {
    const target = document.querySelector(tab.dataset.tabTarget);
    tabContents.forEach((tabContent) => {
      tabContent.classList.remove("active");
    });
    tabs.forEach((tab) => {
      tab.classList.remove("active");
    });
    tab.classList.add("active");
    target.classList.add("active");
  });
});


function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function replaceAll(str) {
  str = str.replace(new RegExp(escapeRegExp('"'), 'g'), "----");
  str = str.replace(new RegExp(escapeRegExp('<space>'), 'g'), "-----");
  str = str.replace(new RegExp(escapeRegExp("'"), 'g'), "------");
  return str
}

function replaceAllMain(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function replaceReverse(str){
  str = str.replace(new RegExp(escapeRegExp("------"), 'g'), "'");
  str = str.replace(new RegExp(escapeRegExp('-----'), 'g'), "<space>");
  str = str.replace(new RegExp(escapeRegExp('----'), 'g'), '"');
  
  return str
}

//MY BLOGS

function myBlogClickTrigger() {
  $("#blogs-cards-container").empty()
  page = 1;
  fetchAllBlogNews(page);
  currentscrollHeight= 0;
  lockScroll= false;
}


$(window).on('scroll', function() {
  const scrollHeight = $(document).height();
  const scrollPos = Math.floor($(window).height() + $(window).scrollTop());
  
  const isBottom = scrollHeight - 300 < scrollPos;
  if (isBottom && currentscrollHeight < scrollHeight)
  {
    page = page + 1;
    fetchAllBlogNews(page);
    currentscrollHeight = scrollHeight;
  }
});

$(document).ready(function (){
  fetchAllBlogNews(page);
});

function fetchAllBlogNews(page=1) {
  const endpoint = `http://${hostname}/api/blogs/news/feed/?page=${page}`;

  fetch(endpoint)
  .then(function (response) {
    return response.json();
  })
  .then(function (response) {
    if (response.data.length < 1) {
      $("#blogs-empty-case").html(
        "You do not have any news yet. Please add blog topics you want to receive news in the \"Add/Remove Blogs\" tab"
      );
    }else {
      $("#blogs-empty-case").empty();
      const newCard = response.data.map((item) => {
        //NEED TO UPDATE IMAGE SOURCE HERE
        dateTimeData = "" + item.disseminated_time
        var timestamp = Date.parse(dateTimeData)
        if (isNaN(timestamp)==false){
          dateTime = new Date(timestamp);
          dateTimeDisplayString = dateTime.toLocaleString("en-in")
        } else {
          var altTimeString = dateTimeData.slice(0,19);
          if (isNaN(altTimeString)){
            dateTime = new Date(altTimeString);
            dateTimeDisplayString = dateTime.toLocaleString("en-in")
          }
          else dateTimeDisplayString = ""
        }
        
        img_html = item.urlToImage ? `<img src="${ item.urlToImage }" class="borders-0 card-img-top px-3 pb-4 my-1" alt="Image">` :
                    item.alternate_img ? `<img src="${item.alternate_img}" class="borders-0 card-img-top px-3 pb-4 my-1" alt="Image"></img>` : "" ;
        
        topics_html = "";
        item.related_topics.map((topic, index)=>{
          topic_url = item.topic_urls[index]
          topics_html += `<a href="${topic_url}"
          class="">
              <h6 class="font-weight-bold d-inline text-uppercase" 
              style="color: #ef073e;">
              ${ topic } | </h6>
            </a>`
        });
        if (item.related_topics.length>0){
          first_topic = item.topic_names[0];
          first_topic_display_name = replaceAllMain(item.related_topics[0]," ","+") ;
        }
        else {
          first_topic = ""
          first_topic_display_name = ""
        }
        source_html = item.source ? `<p class="card-text d-inline"><small class="text-muted">${item.source}&nbsp;&nbsp;&nbsp;&nbsp; ${ dateTimeDisplayString }</small></p>` :
            item.alternate_source ? `<p class="card-text d-inline"><small class="text-muted">${item.source}&nbsp;&nbsp;&nbsp;&nbsp; ${ dateTimeDisplayString }</small></p>` :
            dateTimeDisplayString ? `<p class="card-text d-inline"><small class="text-muted">${ dateTimeDisplayString }</small></p>`: ""
        share_img_url = STATIC_URL+'images/news/share_logo.png' ;
        whatsapp_img_url = STATIC_URL + "images/news/whatsapp_logo_resized.png";
        twitter_img_url = STATIC_URL + "images/news/twitter_logo.png";

        return `
        <div class="infinite-item">
          <div class="row">
            <div class="card mb-3 border-0 shadow-sm" style="box-shadow: 0 0 2px 2px grey; width:100%;">
              <div class="row no-gutters">
                <div class="col-md-3 col-sm-pull-9 order-md-2">
                  ${img_html}
                </div>
                <div class="col-md-9 col-sm-push-3  order-md-1">
                  <div class="card-body">

                    ${topics_html}
                    <a target="__blank" href="${ item.url }" class="">
                      <h5 class="card-title font-weight-bold" style="color: #404145; padding:0px;" >${item.title}</h5>
                    </a>
                    ${source_html}
                    <div class="mb-2" >

                      <a class="text-secondary" target="__blank" href="${ item.url }">more</a>

                    </div>
                    
                      <div class="d-flex justify-content-left">
                        <a class="dv-collapse">
                          <img class="img-fluid" src=" ${share_img_url}" alt="Share" style="height:20px;" /> 
                        </a>
                        
                          <div class="ml-1 dv-collapse-next" >
                            <div class="">
                              <a class="social-share-mobile-only ml-1" href="whatsapp://send?text=${item.url}%0a%0aKeep updated on '${first_topic_display_name}' at ${hostname}/follow/${first_topic}" data-action="share/whatsapp/share">
                              <img class="img-fluid" src="${whatsapp_img_url}" alt="Share" style="height:20px;" /></a>
  
                              <a target="__blank" class="ml-1" href="https://twitter.com/intent/tweet?text=${item.url}%0a${first_topic_display_name}%0a%0aKeep updated on '${first_topic_display_name}' at ${hostname}/follow/${first_topic}" class="twitter-share-button" data-show-count="false">
                              <img class="img-fluid" src="${twitter_img_url}" alt="Tweet" style="height:20px;" /></a>
                            </div>
                          </div>
                        
                      </div>
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>
                  `;
      });
      
      qSelector = document
        .querySelector("#blogs-cards-container")
        qSelector.insertAdjacentHTML("beforeend", newCard.join("\n"));
    }
  })
  .catch(function (err) {
    console.log("error: " + err);
  });
}


// DISCOVER BLOGS
var query,
  blog_topics_present = [];


  // Fetching Followed Companies Once

$("#discover-tab").one("click", function () {
  fetchData(
    `http://${hostname}/api/blogs/topics/getAllFollowing/`,
    renderFollowedBlogs
  );
});

// Render Followed Companies

function renderFollowedBlogs(data) {
  if(data.blog_topics_following_status=="Successful")
  data.blog_topics_following.map((item) => {
    createDiscoverCard(replaceAll(item.name), replaceAll(item.display_name), "Unfollow");
  });
}

const min_query_length = 3;
const spinner = `<div class="spinner"></div>`;

$(document).ready(function () {
  $("#input-field").on("input", function () {
    query = $(this).val();
    if (query.length >= min_query_length) {
      //   Fetching Autocomplete Sugguestions
      fetchData(
        `http://${hostname}/api/blogs/topics/autocomplete/?search=${query}`,
        renderAutocompleteItem
      );
    } else {
      $(".autocomplete").empty();
    }
  });
});

function renderAutocompleteItem(data){
  var filtered_autocomplete_sugguestions = data.filter(
    (item) => !blog_topics_present.includes(replaceAll(item.name))
  );

  var new_autocomplete_item = filtered_autocomplete_sugguestions
    .map((item) => {
      replaced_name = replaceAll(item.name);
      replaced_display_name = replaceAll(item.display_name);
      return `
                      <li class="autocomplete-items"  onclick="createDiscoverCard('${replaced_name}','${replaced_display_name}')">
                         <p class="autocomplete-title"> ${item.display_name}</p>
                          <button id="${replaced_name}" class="add-button">Add</button>
                      </li>
                  `;
    })
    .join("\n");
  $(".autocomplete").empty();

  document
    .querySelector(".autocomplete")
    .insertAdjacentHTML("afterbegin", new_autocomplete_item);
}


function createDiscoverCard(name, display_name, text) {
  recreated = replaceReverse(name)
  display_name = replaceReverse(display_name)
  blog_topics_present.push(name);
  replaced_name = name
  const card = `
                  <div class="discover-card" id="${replaced_name}">
                      <h5 class="card-title"> ${display_name} </h5>
                      <button class="status-button" onclick="removeCard('${name}', this)">
                       ${text || spinner}
                      </button>
                  </div>
              `;

  $(".autocomplete").empty();

  $("#input-field").val("");

  document
    .querySelector("#discover-cards-container")
    .insertAdjacentHTML("afterbegin", card);

  getBlogStatus(recreated);

}

// Fetching Company Status

function getBlogStatus(name) {
  fetchData(
    `http://${hostname}/api/blogs/topics/follow/?blog=${name}`,
    checkStatus
  );
}

function checkStatus(data) {
  if (data.blog_follow_status === "Successful") {
    elem_id = replaceAll(data.blog_name)
    elem = $(document.getElementById(elem_id))
    elem
    .children("button")
      .html("Unfollow");
  } else {
    card = $("#discover-cards-container").children().first();
    card
      .children("button")
      .html("Try again")
      .css("background-color", "#4caf50");
    setTimeout(() => {
      removeGivenCard(card);
    }, 1000);
    blog_topics_present.splice(
      blog_topics_present.indexOf(card.attr("id")),
      1
    );
  }
}

function removeGivenCard(card) {
  card.remove();
}


// Removing Card on Unfollow

function removeCard(name, obj) {
  $(obj).html(spinner);
  blog_topics_present = blog_topics_present.filter(function (e) {
    return e !== name;
  });
  recreated = replaceReverse(name)
  fetchData(
    `http://${hostname}/api/blogs/topics/unfollow/?blog=${recreated}`,
    checkUnfollowStatus
  );
}

function checkUnfollowStatus(data) {
  if (data.blog_unfollow_status === "Successful") {
    elem_id = replaceAll(data.blog_name)
    elem = $(document.getElementById(elem_id))
    elem.remove();
  } else {
    $("#status-button").html("Unfollow");
  }
}

// Fetch Function

function fetchData(endpoint, onSuccess) {
  fetch(endpoint)
    .then(function (response) {
      return response.json();
    })
    .then(function (response) {
      onSuccess(response.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}