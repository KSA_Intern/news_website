function randomColours(){
    var allowedColours = ["#a74393","#525cea","#d74769","#7244a2","#d65142"];
    const random = Math.floor(Math.random() * allowedColours.length);
    return allowedColours[random]
  }

  $(document).ready(function(){
      $(".random-colour").each(function(){
          var colour = randomColours();
          $(this).css("background-color",colour);
      })
  });

  $(function(){
    $(".btn-follow").on('click',function () {
       $(this).text(function(i, text){
           return "Unfollowed";
       })
    //    $(this).css({ 'padding': '7px'});
       $(this).addClass('btn-danger').removeClass('btn-success');
    });
 });