function generate() {

    var hexValues = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e"];
    
    function populate(a) {
      for ( var i = 0; i < 6; i++ ) {
        var x = Math.round( Math.random() * 14 );
        var y = hexValues[x];
        a += y;
      }
      return a;
    }
    
    var newColor1 = populate('#');
    var newColor2 = populate('#');
    var angle = Math.round( Math.random() * 360 );
    
    var gradient = "linear-gradient(" + angle + "deg, " + newColor1 + ", " + newColor2 + ")";
    
    return gradient
    
  }

  $("#dv-autocomplete").autocomplete({
    source: "/news/autocomplete/",
    minLength: 1,
    open: function(){
        setTimeout(function () {
            $('.ui-autocomplete').css('z-index', 999);
        }, 0);
    }
  });

  function randomColours(){
    var allowedColours = ["#a74393","#525cea","#d74769","#7244a2","#d65142"];
    const random = Math.floor(Math.random() * allowedColours.length);
    return allowedColours[random]
  }

  $(document).ready(function(){
      $(".random-colour").each(function(){
          var colour = randomColours();
          $(this).css("background-color",colour);
      })
  });

  $(function(){
    $(".btn-follow").on('click',function () {
       $(this).text(function(i, text){
           return "Following";
       })
       $(this).css({'font-size':'12px'});
    });
 });