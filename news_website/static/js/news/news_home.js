var infinite = new Waypoint.Infinite({
    element: $('.infinite-container')[0],
    offset: function() {
      return 2.5*this.context.innerHeight() - this.adapter.outerHeight()
    },
    onBeforePageLoad: function () {
      $('.loading').show();
    },
    onAfterPageLoad: function ($items) {
      $('.loading').hide();
    }
  });
  
$(function(){
    $(document).on('click',".contentCollapsed",function () {
       $(this).text(function(i, text){
           return text === "more" ? "less" : "more";
       })
    });
 });

 $(function(){
  $(document).off().on('click',".navbar-toggler",function (event) {
    event.preventDefault();
    $header = $(this);
    //getting the next element
    $content = $header.next();
    $content.toggle(350);
  });
});

 $(function(){
  $(document).on('click',".dv-collapse",function (event) {
    event.preventDefault();
    $header = $(this);
    //getting the next element
    $content = $header.next();
    $content.toggle(1);
  });
});

$("#dv-autocomplete").autocomplete({
    source: "/news/autocomplete/",
    minLength: 1,
    open: function(){
        setTimeout(function () {
            $('.ui-autocomplete').css('z-index', 999);
        }, 0);
    }
  });

  function randomColours(){
    var allowedColours = ["#a74393","#525cea","#d74769","#7244a2","#d65142"];
    const random = Math.floor(Math.random() * allowedColours.length);
    return allowedColours[random]
  }

  $(document).ready(function(){
      $(".random-colour").each(function(){
          var colour = randomColours();
          $(this).css("background-color",colour);
      })
  });