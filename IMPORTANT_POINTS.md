- The system (DatabaseFurnisher) ignores the articles if the nlp cannot get any topics out of articles AND also no topic is given to store it under
- The class implementation has the updated code. To add to database or any other such activity use the class based implementation
- In tags of a topic if you are adding multiple tags then separate them by " <space> ". Don't forget the  first and the last space between "<space>". It is very important
- The biggest problem with tags is using tags as div id. Because tags have '<' and '>' in them and id won't take it. So if you are using it as id, make sure you replce '<' and '>' with something else
