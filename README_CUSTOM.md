# News Website Custom Setup Docs

### Author : Darshan D V (darshandv10@gmail.com)

## Local setup

- If you have multiple projects using the same services like redis / multiple docker images using the same conatiner_name change the *_container_name_* parameter in all the services  in the .yml file.
- Copy the production environment varibles from server to ./envs/production and other varibles from server's .env to ./.env
- Check if `./secrets` folder has `certs` and `keystore` folders created
- Provide permission of the current folder to the current user by running `sudo chown -R $USER:$USER .`
- Set up ELK stack. Run `docker-compose -f docker-compose-elk-setup.yml run --rm keystore`.
- Run `docker-compose -f docker-compose-elk-setup.yml run --rm certs` . 
- Start by building the project. Run `docker-compose -f local.yml build`. This builds all the required services.
- Make sure the port specified (8000 here) is free.
- Run `sysctl -w vm.max_map_count=262144` for elasticsearch everytime you restart your system or change permanently.
- Run your initial set up commands if you have new models added as follows
  - - `docker-compose -f local.yml run --rm django python manage.py makemigrations`
  - - `docker-compose -f local.yml run --rm django python manage.py migrate`
- Start all the services by running `docker-compose -f local.yml up`
- Rebuild elasticsearch indices

## Production Setup
- Install docker from [official repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
- Install docker-compose from [official website](https://docs.docker.com/compose/install/)
- If you have multiple projects using the same services like redis / multiple docker images using the same conatiner_name change the *_container_name_* parameter in all the services  in the .yml file.
- Add your website domain and IP into ALLOWED_HOSTS in production.py file of config/settings/ .
- Provide permission of the current folder to the current user by running `sudo chown -R $USER:$USER .`
- Make sure the port specified (5000 here) is free.
- Run `sysctl -w vm.max_map_count=262144` for elasticsearch
- Copy ./envs/production/ and .env folders/files to the production server.
- Check if `./secrets` folder has `certs` and `keystore` folders created
- Change the encryption keys in `./compose/production/kibana/kibana.yml` if necessary to atleast 32 chars/
- Run `docker-compose -f production.yml -f docker-compose-elk-setup.yml run --rm keystore` to generate keystores.
- Run `docker-compose -f production.yml -f docker-compose-elk-setup.yml run --rm certs` to generate certs.
  - - `docker-compose -f production.yml run --rm django python manage.py makemigrations`
  - - `docker-compose -f production.yml run --rm django python manage.py migrate`
- Run `docker-compose -f production.yml run --rm django python manage.py search_index --rebuild` to refresh the index
- Start all the services by running `docker-compose -f production.yml up`

## Databse Backup and Copying
- You can create the backup by running `docker-compose -f <environment>.yml (exec |run --rm) postgres backup`
- This will create the backup in the container under /backups folder with timeframe.
- You can look at the backups using the command `docker-compose -f <environment>.yml (exec |run --rm) postgres backups` 
- Let's say it created a backup file */backups/backup_2020_10_22T13_11_47.sql.gz*.
- You can copy the backup to your local server (file system) using `docker cp <container hash eg:191a6a9fce08>:/backups/backup_2020_10_22T13_11_47.sql.gz <target_dir>` where `<container hash>` is the hash of the postgres container and `<target_dir>` is where you want to copy the backup to.
- Similarly, after copying the database backup to the container, database can be restored by `docker-compose -f <environment>.yml run --rm postgres restore backup_2020_10_22T13_11_47.sql.gz`
- To login to postres `docker-compose -f production.yml run --rm postgres psql -h postgres -U debug news_website`

NOTE :
- If there is any error when any docker command is run. Restart docker service by running `sudo systemctl restart docker.service`. Later run the docker or docker-compose command
- While developing please make sure you don't push database to make exceptions (Eg: Trying to create a new object with same _unique_ parameters even though it already exists) because postgres image sometimes fails if it has to handle any database exceptions at database level.
